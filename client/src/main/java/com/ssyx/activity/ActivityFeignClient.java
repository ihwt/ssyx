package com.ssyx.activity;

import com.ssyx.model.activity.CouponInfo;
import com.ssyx.model.order.CartInfo;
import com.ssyx.vo.order.CartInfoVo;
import com.ssyx.vo.order.OrderConfirmVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@FeignClient("service-activity")
public interface ActivityFeignClient {
	
	// 获取购物车里面满足条件优惠卷和活动的信息
	@ApiOperation("根据购物车获取促销信息")
	@PostMapping("/api/activity/inner/findCartActivityAndCoupon/{userId}")
	OrderConfirmVo findCartActivityAndCoupon(@RequestBody List<CartInfo> cartInfoList,
	                                         @PathVariable("userId") Long userId);
	
	// 根据skuID获取营销数据和优惠卷
	@ApiOperation("根据skuID获取营销数据和优惠卷")
	@GetMapping("/api/activity/inner/findActivityAndCoupon/{skuId}/{userId}")
	Map<String, Object> findActivityAndCoupon(@PathVariable("skuId") Long skuId,
	                                          @PathVariable("userId") Long userId);
	
	// 根据skuID获取活动信息
	@ApiOperation("根据skuID获取活动信息")
	@PostMapping("/api/activity/inner/findActivity")
	Map<Long, List<String>> findActivity(@RequestBody List<Long> skuIdList);
	
	// 根据优惠券ID获取优惠券对应商品的ID
	@ApiOperation("根据优惠券ID获取优惠券对应商品的ID")
	@PostMapping("/api/activity/inner/findRangeSkuIdList/{couponId}")
	CouponInfo findRangeSkuIdList(@RequestBody List<CartInfo> cartInfoList,
	                              @PathVariable("couponId") Long couponId);
	
	// 更新优惠券使用状态
	@ApiOperation("更新优惠券使用状态")
	@GetMapping("/api/activity/inner/updateCouponInfoUseStatus/{couponId}/{userId}/{orderId}")
	Boolean updateCouponInfoUseStatus(@PathVariable("couponId") Long couponId,
	                                  @PathVariable("userId") Long userId,
	                                  @PathVariable("orderId") Long orderId);
	
	// 获取购物车对应规则数据
	@ApiOperation("根据活动id获取活动skuid列表")
	@PostMapping("/api/activity/inner/findCartActivityList")
	List<CartInfoVo> findCartActivityList(@RequestBody List<CartInfo> cartInfoList);
	
	// 更新优惠券支付时间
	@ApiOperation("更新优惠券支付时间")
	@GetMapping(value = "/api/activity/inner/updateCouponInfoUsedTime/{couponId}/{userId}")
	Boolean updateCouponInfoUsedTime(@PathVariable("couponId") Long couponId, @PathVariable("userId") Long userId);
	
	@ApiOperation("根据活动id获取活动skuid列表")
	@GetMapping(value = "/api/activity/inner/findSkuIdList/{activityId}")
	List<Long> findSkuIdList(@PathVariable("activityId") Long activityId);
}
