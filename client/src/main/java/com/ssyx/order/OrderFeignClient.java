package com.ssyx.order;

import com.ssyx.model.order.OrderInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("service-order")
public interface OrderFeignClient {
	
	// 获取订单信息
	@GetMapping("/api/order/inner/getOrderInfo/{orderNo}")
	OrderInfo getOrderInfo(@PathVariable("orderNo") String orderNo);
}
