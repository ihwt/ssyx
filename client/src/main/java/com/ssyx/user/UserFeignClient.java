package com.ssyx.user;

import com.ssyx.vo.user.LeaderAddressVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("service-user")
public interface UserFeignClient {
	
	// 根据userId查询提货点和团长信息
	@GetMapping("/api/user/leader/inner/getUserAddressByUserId/{userId}")
	LeaderAddressVo getLeaderAddressVoByUserId(@PathVariable("userId") Long userId);
	
}

