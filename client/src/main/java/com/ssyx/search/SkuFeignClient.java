package com.ssyx.search;

import com.ssyx.model.search.SkuEs;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("service-search")
public interface SkuFeignClient {
	
	// 更新商品热度
	@GetMapping("/api/search/sku/inner/incrHotScore/{skuId}")
	Boolean incrHotScore(@PathVariable("skuId") Long skuId);
	
	// 查询商品热度
	@GetMapping("/api/search/sku/inner/findHotSkuList")
	List<SkuEs> findHotSkuList();
	
}
