package com.ssyx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.model.product.SkuInfo;
import com.ssyx.result.Result;
import com.ssyx.service.SkuInfoService;
import com.ssyx.vo.product.SkuInfoQueryVo;
import com.ssyx.vo.product.SkuInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuInfoController
 * @description: 商品SKU管理类
 * @data: 2024/3/11 16:12
 * @author: ihu
 * @version: 1.0
 */

@Slf4j
@Api(tags = "商品Sku管理")
@RestController("SkuInfoController")
@RequestMapping(value = "/admin/product/skuInfo")
public class SkuInfoController {
	
	@Resource
	private SkuInfoService skuInfoService;
	
	@ApiOperation(value = "获取sku分页列表")
	@GetMapping("{page}/{limit}")
	public Result<IPage<SkuInfo>> index(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			
			@ApiParam(name = "skuInfoQueryVo", value = "查询对象", required = false)
			SkuInfoQueryVo skuInfoQueryVo) {
		log.info("获取角色分页列表：page:{},limit:{},skuInfoQueryVo:{}", page, limit, skuInfoQueryVo);
		Page<SkuInfo> pageParam = new Page<>(page, limit);
		return Result.ok(skuInfoService.selectPage(pageParam, skuInfoQueryVo));
	}
	
	/**
	 * 添加商品
	 */
	@ApiOperation(value = "新增商品")
	@PostMapping("save")
	public Result<?> save(@RequestBody SkuInfoVo skuInfoVo) {
		log.info("新增商品：skuInfoVo:{}", skuInfoVo);
		skuInfoService.saveSkuInfo(skuInfoVo);
		return Result.ok(null);
	}
	
	/**
	 * 修改商品
	 */
	@ApiOperation(value = "修改商品")
	@PutMapping("update")
	public Result<?> updateById(@RequestBody SkuInfoVo skuInfoVo) {
		log.info("修改商品：skuInfoVo:{}", skuInfoVo);
		skuInfoService.updateSkuInfo(skuInfoVo);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "根据id获取商品")
	@GetMapping("get/{id}")
	public Result<SkuInfoVo> get(@PathVariable Long id) {
		log.info("根据id获取商品：id:{}", id);
		return Result.ok(skuInfoService.getSkuInfoVo(id));
	}
	
	@ApiOperation(value = "删除商品")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("根据id删除商品：id:{}", id);
		skuInfoService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "批量删除商品")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("批量删除商品：idList:{}", idList);
		skuInfoService.removeByIds(idList);
		return Result.ok(null);
	}
	
	/**
	 * 商品审核
	 */
	@ApiOperation(value = "商品审核状态")
	@GetMapping("check/{skuId}/{status}")
	public Result<?> check(@PathVariable("skuId") Long skuId, @PathVariable("status") Integer status) {
		log.info("商品审核状态：skuId:{},status:{}", skuId, status);
		skuInfoService.update(Wrappers.<SkuInfo>lambdaUpdate().eq(SkuInfo::getId, skuId).set(SkuInfo::getCheckStatus,
				status));
		return Result.ok(null);
	}
	
	/**
	 * 商品上架
	 */
	@ApiOperation(value = "上架商品")
	@GetMapping("publish/{skuId}/{status}")
	public Result<?> publish(@PathVariable("skuId") Long skuId,
	                         @PathVariable("status") Integer status) {
		log.info("上架商品：skuId:{},status:{}", skuId, status);
		skuInfoService.publish(skuId, status);
		return Result.ok(null);
	}
	
	/**
	 * 新人专享
	 */
	@ApiOperation(value = "新人专享")
	@GetMapping("isNewPerson/{skuId}/{status}")
	public Result<?> isNewPerson(@PathVariable("skuId") Long skuId,
	                             @PathVariable("status") Integer status) {
		log.info("新人专享：skuId:{},status:{}", skuId, status);
		skuInfoService.update(Wrappers.<SkuInfo>lambdaUpdate().eq(SkuInfo::getId, skuId).set(SkuInfo::getIsNewPerson,
				status));
		return Result.ok(null);
	}
}
