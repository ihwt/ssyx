package com.ssyx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.annotation.AutoType;
import com.ssyx.constant.GlobalConstant;
import com.ssyx.model.product.Category;
import com.ssyx.result.Result;
import com.ssyx.service.CategoryService;
import com.ssyx.vo.product.CategoryQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: CategoryController
 * @description: 商品分类
 * @data: 2024/3/11 12:57
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "商品分类管理")
@RestController("CategoryController")
@RequestMapping("/admin/product/category")
@SuppressWarnings({"unchecked", "rawtypes"})
public class CategoryController {
	
	@Resource
	private CategoryService categoryService;
	
	@ApiOperation(value = "获取分页列表")
	@GetMapping("{page}/{limit}")
	public Result<IPage<Category>> index(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			@ApiParam(name = "categoryQueryVo", value = "查询对象", required = false)
			CategoryQueryVo categoryQueryVo) {
		log.info("获取角色分页列表：page:{},limit:{},categoryQueryVo:{}", page, limit, categoryQueryVo);
		Page<Category> pageParam = new Page<>(page, limit);
		return Result.ok(categoryService.selectPage(pageParam, categoryQueryVo));
	}
	
	@ApiOperation(value = "获取商品分类")
	@GetMapping("get/{id}")
	public Result get(@PathVariable Long id) {
		log.info("获取角色信息：id:{}", id);
		return Result.ok(categoryService.getById(id));
	}
	
	@AutoType(value = "Category", tags = "新增商品分类")
	@ApiOperation(value = "新增商品分类")
	@PostMapping("save")
	public Result save(@RequestBody Category category) {
		log.info("新增商品分类：category:{}", category);
		category.setStatus(GlobalConstant.INTEGER_ONE);
		categoryService.save(category);
		return Result.ok(null);
	}
	
	@AutoType(value = "Category", tags = "修改商品分类")
	@ApiOperation(value = "修改商品分类")
	@PutMapping("update")
	public Result update(@RequestBody Category category) {
		log.info("修改商品分类：category:{}", category);
		categoryService.updateById(category);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "删除商品分类")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("删除商品分类：id:{}", id);
		categoryService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "批量删除商品分类")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("批量删除商品分类：idList:{}", idList);
		categoryService.removeByIds(idList);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "获取全部商品分类")
	@GetMapping("findAllList")
	public Result<List<Category>> findAllList() {
		log.info("获取全部商品分类");
		return Result.ok(categoryService.list());
	}
}