package com.ssyx.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ssyx.annotation.AutoType;
import com.ssyx.model.product.Attr;
import com.ssyx.result.Result;
import com.ssyx.service.AttrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: AttrController
 * @description: 平台管理类
 * @data: 2024/3/11 15:39
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "平台管理")
@RestController("AttrController")
@RequestMapping(value = "/admin/product/attr")
public class AttrController {
	
	@Resource
	private AttrService attrService;
	
	@ApiOperation(value = "获取全部平台")
	@GetMapping("{attrGroupId}")
	public Result<List<Attr>> index(
			@ApiParam(name = "attrGroupId", value = "分组id", required = true)
			@PathVariable Long attrGroupId) {
		log.info("获取全部平台：attrGroupId:{}", attrGroupId);
		return Result.ok(attrService.list(Wrappers.<Attr>lambdaQuery().eq(Attr::getAttrGroupId, attrGroupId)));
	}
	
	@ApiOperation(value = "根据id获取平台")
	@GetMapping("get/{id}")
	public Result<Attr> get(@PathVariable Long id) {
		log.info("根据id获取平台：id:{}", id);
		return Result.ok(attrService.getById(id));
	}
	
	@AutoType(value = "Attr", tags = "新增平台")
	@ApiOperation(value = "新增平台")
	@PostMapping("save")
	public Result<?> save(@RequestBody Attr attr) {
		log.info("新增平台：attr:{}", attr);
		attrService.save(attr);
		return Result.ok(null);
	}
	
	@AutoType(value = "Attr", tags = "修改平台")
	@ApiOperation(value = "修改平台")
	@PutMapping("update")
	public Result<?> updateById(@RequestBody Attr attr) {
		log.info("修改平台：attr:{}", attr);
		attrService.updateById(attr);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "删除平台")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("删除平台：id:{}", id);
		attrService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "批量删除平台")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("批量删除平台：idList:{}", idList);
		attrService.removeByIds(idList);
		return Result.ok(null);
	}
}
