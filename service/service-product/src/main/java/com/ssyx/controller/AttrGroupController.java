package com.ssyx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.annotation.AutoType;
import com.ssyx.model.product.AttrGroup;
import com.ssyx.result.Result;
import com.ssyx.service.AttrGroupService;
import com.ssyx.vo.product.AttrGroupQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: AttrGroupController
 * @description: 平台分组类
 * @data: 2024/3/11 14:49
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "平台分组管理")
@RestController("AttrGroupController")
@RequestMapping("/admin/product/attrGroup")
public class AttrGroupController {
	
	@Resource
	private AttrGroupService attrGroupService;
	
	@ApiOperation(value = "获取分页列表")
	@GetMapping("{page}/{limit}")
	public Result<IPage<AttrGroup>> index(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			
			@ApiParam(name = "attrGroupQueryVo", value = "查询对象", required = false)
			AttrGroupQueryVo attrGroupQueryVo) {
		log.info("获取角色分页列表：page:{},limit:{},attrGroupQueryVo:{}", page, limit, attrGroupQueryVo);
		Page<AttrGroup> pageParam = new Page<>(page, limit);
		return Result.ok(attrGroupService.selectPage(pageParam, attrGroupQueryVo));
	}
	
	@ApiOperation(value = "根据id获取分组")
	@GetMapping("get/{id}")
	public Result<AttrGroup> get(@PathVariable Long id) {
		log.info("根据id获取分组信息：id{}", id);
		return Result.ok(attrGroupService.getById(id));
	}
	
	@AutoType(value = "AttrGroup", tags = "新增分组")
	@ApiOperation(value = "新增分组")
	@PostMapping("save")
	public Result<?> save(@RequestBody AttrGroup attrGroup) {
		log.info("新增分组：attrGroup:{}", attrGroup);
		attrGroupService.save(attrGroup);
		return Result.ok(null);
	}
	
	@AutoType(value = "AttrGroup", tags = "修改分组")
	@ApiOperation(value = "修改分组")
	@PutMapping("update")
	public Result<?> update(@RequestBody AttrGroup attrGroup) {
		log.info("修改分组：attrGroup:{}", attrGroup);
		attrGroupService.updateById(attrGroup);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "删除分组")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("删除分组：id{}", id);
		attrGroupService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "批量删除分组")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("批量删除分组：idList:{}", idList);
		attrGroupService.removeByIds(idList);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "获取全部分组")
	@GetMapping("findAllList")
	public Result<List<AttrGroup>> findAllList() {
		log.info("获取全部属性分组");
		return Result.ok(attrGroupService.list());
	}
}
