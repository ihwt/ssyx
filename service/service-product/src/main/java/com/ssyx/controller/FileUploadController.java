package com.ssyx.controller;

import com.ssyx.result.Result;
import com.ssyx.service.FileUploadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @program: ssyx-parent
 * @className: FileUploadController
 * @description: 文件上传类
 * @data: 2024/3/12 20:04
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "文件上传")
@RestController("FileUploadController")
@RequestMapping("admin/product")
public class FileUploadController {
	
	@Resource
	private FileUploadService fileUploadService;
	
	//文件上传
	@ApiOperation(value = "文件上传")
	@PostMapping("fileUpload")
	public Result<String> fileUpload(MultipartFile file) {
		return Result.ok(fileUploadService.fileUpload(file));
	}
}
