package com.ssyx.api;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ssyx.model.product.Category;
import com.ssyx.model.product.SkuInfo;
import com.ssyx.service.CategoryService;
import com.ssyx.service.SkuInfoService;
import com.ssyx.vo.product.SkuInfoVo;
import com.ssyx.vo.product.SkuStockLockVo;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: ProductInnnerController
 * @description: 商品远程搜索类
 * @data: 2024/3/15 13:59
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@RestController("ProductInnnerController")
@RequestMapping("/api/product")
public class ProductInnnerController {
	
	@Resource
	private CategoryService categoryService;
	@Resource
	private SkuInfoService skuInfoService;
	
	/**
	 * 根据分类id获取分类信息
	 */
	@ApiOperation("根据分类id获取分类信息")
	@GetMapping("inner/getCategory/{categoryId}")
	public Category getCategory(@PathVariable Long categoryId) {
		log.info("FeignClient(根据分类id获取分类信息)：{}", categoryId);
		return categoryService.getById(categoryId);
	}
	
	/**
	 * 根据skuId获取sku信息
	 */
	@ApiOperation("根据skuId获取sku信息")
	@GetMapping("inner/getSkuInfo/{skuId}")
	public SkuInfo getSkuInfo(@PathVariable("skuId") Long skuId) {
		log.info("FeignClient(根据skuId获取sku信息)：{}", skuId);
		return skuInfoService.getById(skuId);
	}
	
	/**
	 * 根据skuId列表得到sku信息列表
	 */
	@ApiOperation("根据skuId列表得到sku信息列表")
	@PostMapping("inner/findSkuInfoList")
	public List<SkuInfo> findSkuInfoList(@RequestBody List<Long> skuIdList) {
		log.info("FeignClient(根据skuId列表得到sku信息列表)：{}", skuIdList);
		return skuInfoService.listByIds(skuIdList);
	}
	
	
	/**
	 * 根据分类id获取分类列表
	 */
	@ApiOperation("根据分类id获取分类列表")
	@PostMapping("inner/findCategoryList")
	public List<Category> findCategoryList(@RequestBody List<Long> categoryIdList) {
		log.info("FeignClient(根据分类id获取分类列表)：{}", categoryIdList);
		return categoryService.listByIds(categoryIdList);
	}
	
	/**
	 * 根据关键字匹配sku列表
	 */
	@ApiOperation("根据关键字匹配sku列表")
	@GetMapping("inner/findSkuInfoByKeyword/{keyword}")
	public List<SkuInfo> findSkuInfoByKeyword(@PathVariable("keyword") String keyword) {
		log.info("FeignClient(根据关键字匹配sku列表)：{}", keyword);
		return skuInfoService.list(Wrappers.<SkuInfo>lambdaQuery().like(SkuInfo::getSkuName, keyword));
	}
	
	/**
	 * 获取所有分类
	 */
	@ApiOperation("获取所有分类")
	@GetMapping("inner/findAllCategoryList")
	public List<Category> findAllCategoryList() {
		log.info("FeignClient(获取所有分类)");
		return categoryService.list();
	}
	
	/**
	 * 获取新人专享商品
	 */
	@ApiOperation("获取新人专享商品")
	@GetMapping("inner/findNewPersonSkuInfoList")
	public List<SkuInfo> findNewPersonSkuInfoList() {
		log.info("FeignClient(获取新人专享商品)");
		return skuInfoService.findNewPersonSkuInfoList();
	}
	
	/**
	 * 根据skuId获取sku信息
	 */
	@ApiOperation("根据skuId获取sku信息")
	@GetMapping("inner/getSkuInfoVo/{skuId}")
	public SkuInfoVo getSkuInfoVo(@PathVariable Long skuId) {
		log.info("FeignClient(根据skuId获取sku信息)：{}", skuId);
		return skuInfoService.getSkuInfoVo(skuId);
	}
	
	/**
	 * 验证和锁定库存
	 */
	@ApiOperation("验证和锁定库存")
	@PostMapping("inner/checkAndLock/{orderNo}")
	public Boolean checkAndLock(@RequestBody List<SkuStockLockVo> skuStockLockVoList,
	                            @PathVariable String orderNo) {
		log.info("FeignClient(验证和锁定库存)：{}", skuStockLockVoList);
		return skuInfoService.checkAndLock(skuStockLockVoList, orderNo);
	}
	
}
