package com.ssyx.receiver;

import com.rabbitmq.client.Channel;
import com.ssyx.constant.RabbitMQConstant;
import com.ssyx.service.SkuInfoService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;

@Component
public class StockReceiver {
	
	@Resource
	private SkuInfoService skuInfoService;
	
	/**
	 * 扣减库存成功，更新订单状态
	 */
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = RabbitMQConstant.QUEUE_MINUS_STOCK, durable = "true"),
			exchange = @Exchange(RabbitMQConstant.EXCHANGE_ORDER_DIRECT),
			key = {RabbitMQConstant.ROUTING_MINUS_STOCK}
	))
	public void minusStock(String orderNo,
	                       Message message,
	                       Channel channel) throws IOException {
		if (!StringUtils.isEmpty(orderNo)) {
			skuInfoService.minusStock(orderNo);
		}
		channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
	}
}
