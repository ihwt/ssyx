package com.ssyx;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SsyxServiceProductApplication {
	public static void main(String[] args) {
		SpringApplication.run(SsyxServiceProductApplication.class, args);
	}
}
