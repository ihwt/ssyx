package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.product.SkuImage;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: SkuImageMapper
 * @description: 接口
 * @data: 2024/3/12 20:23
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface SkuImageMapper extends BaseMapper<SkuImage> {
}
