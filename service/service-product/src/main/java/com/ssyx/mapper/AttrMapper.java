package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.product.Attr;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: AttrMapper
 * @description: 接口
 * @data: 2024/3/11 15:41
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface AttrMapper extends BaseMapper<Attr> {
}
