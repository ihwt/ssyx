package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.product.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: CategoryMapper
 * @description: 商品分类
 * @data: 2024/3/11 13:00
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
