package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.product.SkuPoster;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: skuPosterMapper
 * @description: 接口
 * @data: 2024/3/12 20:24
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface skuPosterMapper extends BaseMapper<SkuPoster> {
}
