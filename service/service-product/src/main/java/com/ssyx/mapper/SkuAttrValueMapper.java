package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.product.SkuAttrValue;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: SkuAttrValueMapper
 * @description: 接口
 * @data: 2024/3/12 20:21
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface SkuAttrValueMapper extends BaseMapper<SkuAttrValue> {
}
