package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.product.AttrGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: AttrGroupMapper
 * @description: 接口
 * @data: 2024/3/11 14:51
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface AttrGroupMapper extends BaseMapper<AttrGroup> {
}
