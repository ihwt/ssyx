package com.ssyx.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @program: ssyx-parent
 * @className: FileUploadService
 * @description: 文件上传接口
 * @data: 2024/3/12 20:05
 * @author: ihu
 * @version: 1.0
 **/

public interface FileUploadService {
	String fileUpload(MultipartFile file);
}
