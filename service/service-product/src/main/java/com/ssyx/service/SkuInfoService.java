package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.product.SkuInfo;
import com.ssyx.vo.product.SkuInfoQueryVo;
import com.ssyx.vo.product.SkuInfoVo;
import com.ssyx.vo.product.SkuStockLockVo;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuInfoService
 * @description: 接口
 * @data: 2024/3/11 16:14
 * @author: ihu
 * @version: 1.0
 **/

public interface SkuInfoService extends IService<SkuInfo> {
	
	// 获取sku分页列表
	IPage<SkuInfo> selectPage(Page<SkuInfo> pageParam, SkuInfoQueryVo skuInfoQueryVo);
	
	void saveSkuInfo(SkuInfoVo skuInfoVo);
	
	SkuInfoVo getSkuInfoVo(Long id);
	
	void updateSkuInfo(SkuInfoVo skuInfoVo);
	
	void publish(Long skuId, Integer status);
	
	Boolean checkAndLock(List<SkuStockLockVo> skuStockLockVoList, String orderNo);
	
	List<SkuInfo> findNewPersonSkuInfoList();
	
	void minusStock(String orderNo);
}
