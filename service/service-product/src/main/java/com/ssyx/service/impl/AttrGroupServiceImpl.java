package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.mapper.AttrGroupMapper;
import com.ssyx.model.product.AttrGroup;
import com.ssyx.service.AttrGroupService;
import com.ssyx.vo.product.AttrGroupQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @program: ssyx-parent
 * @className: AttrGroupServiceImpl
 * @description: 类
 * @data: 2024/3/11 14:51
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupMapper, AttrGroup> implements AttrGroupService {
	
	// 平台属性分组列表
	@Override
	public IPage<AttrGroup> selectPage(Page<AttrGroup> pageParam, AttrGroupQueryVo attrGroupQueryVo) {
		String name = attrGroupQueryVo.getName();
		return baseMapper.selectPage(pageParam, Wrappers.<AttrGroup>lambdaQuery().like(!StringUtils.isEmpty(name),
				AttrGroup::getName, name));
	}
	
}
