package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.mapper.SkuAttrValueMapper;
import com.ssyx.model.product.SkuAttrValue;
import com.ssyx.service.SkuAttrValueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuAttrValueServiceImpl
 * @description: 属性类
 * @data: 2024/3/12 20:21
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class SkuAttrValueServiceImpl extends ServiceImpl<SkuAttrValueMapper, SkuAttrValue> implements SkuAttrValueService {
	@Override
	public List<SkuAttrValue> findBySkuId(Long skuId) {
		return list(Wrappers.<SkuAttrValue>lambdaQuery().eq(SkuAttrValue::getSkuId, skuId));
	}
}
