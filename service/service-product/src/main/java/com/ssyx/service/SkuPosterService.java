package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.product.SkuPoster;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuPosterService
 * @description: 海报接口
 * @data: 2024/3/12 19:49
 * @author: ihu
 * @version: 1.0
 **/

public interface SkuPosterService extends IService<SkuPoster> {
	List<SkuPoster> findBySkuId(Long skuId);
}
