package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.product.SkuImage;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuImageService
 * @description: 图片接口
 * @data: 2024/3/12 19:49
 * @author: ihu
 * @version: 1.0
 **/

public interface SkuImageService extends IService<SkuImage> {
	List<SkuImage> findBySkuId(Long skuId);
}
