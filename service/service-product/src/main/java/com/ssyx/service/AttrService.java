package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.product.Attr;

/**
 * @program: ssyx-parent
 * @className: AttrService
 * @description: 接口
 * @data: 2024/3/11 15:40
 * @author: ihu
 * @version: 1.0
 **/

public interface AttrService extends IService<Attr> {
}