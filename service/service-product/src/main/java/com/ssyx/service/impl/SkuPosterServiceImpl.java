package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.mapper.skuPosterMapper;
import com.ssyx.model.product.SkuPoster;
import com.ssyx.service.SkuPosterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuPosterServiceImpl
 * @description: 海报类
 * @data: 2024/3/12 20:23
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class SkuPosterServiceImpl extends ServiceImpl<skuPosterMapper, SkuPoster> implements SkuPosterService {
	@Override
	public List<SkuPoster> findBySkuId(Long skuId) {
		return list(Wrappers.<SkuPoster>lambdaQuery().eq(SkuPoster::getSkuId, skuId));
	}
}
