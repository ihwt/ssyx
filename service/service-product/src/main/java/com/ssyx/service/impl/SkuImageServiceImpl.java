package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.mapper.SkuImageMapper;
import com.ssyx.model.product.SkuImage;
import com.ssyx.service.SkuImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuImageServiceImpl
 * @description: 图片类
 * @data: 2024/3/12 20:22
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class SkuImageServiceImpl extends ServiceImpl<SkuImageMapper, SkuImage> implements SkuImageService {
	@Override
	public List<SkuImage> findBySkuId(Long skuId) {
		return list(Wrappers.<SkuImage>lambdaQuery().eq(SkuImage::getSkuId, skuId));
	}
}
