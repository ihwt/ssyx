package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.product.AttrGroup;
import com.ssyx.vo.product.AttrGroupQueryVo;

/**
 * @program: ssyx-parent
 * @className: AttrGroupService
 * @description: 接口
 * @data: 2024/3/11 14:50
 * @author: ihu
 * @version: 1.0
 **/

public interface AttrGroupService extends IService<AttrGroup> {
	
	//平台属性分组列表
	IPage<AttrGroup> selectPage(Page<AttrGroup> pageParam, AttrGroupQueryVo attrGroupQueryVo);
}