package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.product.SkuAttrValue;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuAttrValueService
 * @description: 属性类
 * @data: 2024/3/12 19:50
 * @author: ihu
 * @version: 1.0
 **/

public interface SkuAttrValueService extends IService<SkuAttrValue> {
	List<SkuAttrValue> findBySkuId(Long skuId);
}
