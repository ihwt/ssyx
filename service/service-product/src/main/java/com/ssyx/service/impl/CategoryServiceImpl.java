package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.mapper.CategoryMapper;
import com.ssyx.model.product.Category;
import com.ssyx.service.CategoryService;
import com.ssyx.vo.product.CategoryQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @program: ssyx-parent
 * @className: CategoryServiceImpl
 * @description:
 * @data: 2024/3/11 12:59
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
	
	// 商品分类分页列表
	@Override
	public IPage<Category> selectPage(Page<Category> pageParam, CategoryQueryVo categoryQueryVo) {
		String name = categoryQueryVo.getName();
		return baseMapper.selectPage(pageParam, Wrappers.<Category>lambdaQuery().like(!StringUtils.isEmpty(name),
				Category::getName, name));
	}
	
}
