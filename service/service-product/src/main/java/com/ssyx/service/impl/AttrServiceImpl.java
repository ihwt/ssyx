package com.ssyx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.mapper.AttrMapper;
import com.ssyx.model.product.Attr;
import com.ssyx.service.AttrService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @program: ssyx-parent
 * @className: AttrServiceImpl
 * @description: 类
 * @data: 2024/3/11 15:41
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class AttrServiceImpl extends ServiceImpl<AttrMapper, Attr> implements AttrService {
}
