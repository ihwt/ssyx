package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.product.Category;
import com.ssyx.vo.product.CategoryQueryVo;

/**
 * @program: ssyx-parent
 * @className: CategoryService
 * @description: 商品分类
 * @data: 2024/3/11 12:59
 * @author: ihu
 * @version: 1.0
 **/

public interface CategoryService extends IService<Category> {
	
	//商品分类分页列表
	IPage<Category> selectPage(Page<Category> pageParam, CategoryQueryVo categoryQueryVo);
}
