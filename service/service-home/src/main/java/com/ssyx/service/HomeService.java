package com.ssyx.service;

import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: HomeService
 * @description: 接口
 * @data: 2024/3/18 14:36
 * @author: ihu
 * @version: 1.0
 **/

public interface HomeService {
	Map<String, Object> index(Long id);
}
