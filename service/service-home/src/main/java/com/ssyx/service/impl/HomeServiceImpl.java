package com.ssyx.service.impl;


import com.ssyx.activity.ActivityFeignClient;
import com.ssyx.model.product.Category;
import com.ssyx.model.product.SkuInfo;
import com.ssyx.product.ProductFeignClient;
import com.ssyx.search.SkuFeignClient;
import com.ssyx.service.HomeService;
import com.ssyx.user.UserFeignClient;
import com.ssyx.vo.user.LeaderAddressVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: HomeServiceImpl
 * @description: 类
 * @data: 2024/3/18 14:36
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
@SuppressWarnings({"unchecked", "rawtypes"})
public class HomeServiceImpl implements HomeService {
	
	@Resource
	private ProductFeignClient productFeignClient;
	@Resource
	private UserFeignClient userFeignClient;
	@Resource
	private SkuFeignClient skuFeignClient;
	@Resource
	private ActivityFeignClient activityFeignClient;
	
	@Override
	public Map<String, Object> index(Long userId) {
		Map<String, Object> result = new HashMap<>();
		// 获取分类信息
		List<Category> categoryList = productFeignClient.findAllCategoryList();
		result.put("categoryList", categoryList);
		// 获取新人专享商品
		List<SkuInfo> newPersonSkuInfoList = productFeignClient.findNewPersonSkuInfoList();
		result.put("newPersonSkuInfoList", newPersonSkuInfoList);
		// 提货点地址信息
		LeaderAddressVo leaderAddressVo = userFeignClient.getLeaderAddressVoByUserId(userId);
		result.put("leaderAddressVo", leaderAddressVo);
		// 获取爆品商品
//		List<SkuEs> hotSkuList = skuFeignClient.findHotSkuList();
		//获取sku对应的促销活动标签
//		if (!CollectionUtils.isEmpty(hotSkuList)) {
//			List<Long> skuIdList = hotSkuList.stream().map(SkuEs::getId).collect(Collectors.toList());
//			Map<Long, List<String>> skuIdToRuleListMap = activityFeignClient.findActivity(skuIdList);
//			if (skuIdToRuleListMap != null) {
//				hotSkuList.forEach(skuEs -> {
//					skuEs.setRuleList(skuIdToRuleListMap.get(skuEs.getId()));
//				});
//			}
//		}
//		result.put("hotSkuList", hotSkuList);
		return result;
	}
}
