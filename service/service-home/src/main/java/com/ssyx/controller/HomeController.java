package com.ssyx.controller;

/**
 * @program: ssyx-parent
 * @className: HomeController
 * @description: 首页类
 * @data: 2024/3/18 14:34
 * @author: ihu
 * @version: 1.0
 **/

import com.ssyx.constant.GlobalConstant;
import com.ssyx.result.Result;
import com.ssyx.service.HomeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j
@Api(tags = "首页接口")
@RestController("HomeController")
@RequestMapping("api/home")
public class HomeController {
	
	@Resource
	private HomeService homeService;
	
	@ApiOperation("获取首页数据")
	@GetMapping("index")
	public Result<Map<String, Object>> index(HttpServletRequest request) {
		log.info("请求首页数据");
		// todo return Result.ok(homeService.index(AuthThreadLocal.getId()));
		return Result.ok(homeService.index(GlobalConstant.LONG_ONE));
	}
}