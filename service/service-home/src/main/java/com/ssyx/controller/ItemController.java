package com.ssyx.controller;

import com.ssyx.auth.AuthThreadLocal;
import com.ssyx.result.Result;
import com.ssyx.service.ItemService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Api(tags = "商品详情")
@RestController("ItemController")
@RequestMapping("api/home")
public class ItemController {
	
	@Resource
	private ItemService itemService;
	
	@GetMapping("item/{id}")
	public Result<Map<String, Object>> index(@PathVariable Long id) {
		log.info("商品详情：用户id:{}商品id:{},", AuthThreadLocal.getId(), id);
		return Result.ok(itemService.item(id, AuthThreadLocal.getId()));
	}
}
