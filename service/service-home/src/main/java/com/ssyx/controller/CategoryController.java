package com.ssyx.controller;

import com.ssyx.model.product.Category;
import com.ssyx.product.ProductFeignClient;
import com.ssyx.result.Result;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Api(tags = "商品分类")
@RestController
@RequestMapping("api/home")
public class CategoryController {
	
	@Resource
	private ProductFeignClient productFeignClient;
	
	//查询所有分类
	@GetMapping("category")
	public Result<List<Category>> categoryList() {
		log.info("查询所有分类");
		return Result.ok(productFeignClient.findAllCategoryList());
	}
}
