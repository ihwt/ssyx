package com.ssyx.receiver;

import com.rabbitmq.client.Channel;
import com.ssyx.constant.RabbitMQConstant;
import com.ssyx.service.OrderInfoService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;

@Component
public class OrderReceiver {
	
	@Resource
	private OrderInfoService orderInfoService;
	
	//订单支付成功，更新订单状态，扣减库存
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = RabbitMQConstant.QUEUE_ORDER_PAY, durable = "true"),
			exchange = @Exchange(RabbitMQConstant.EXCHANGE_PAY_DIRECT),
			key = {RabbitMQConstant.ROUTING_PAY_SUCCESS}
	))
	public void orderPay(String orderNo,
	                     Message message,
	                     Channel channel) throws IOException {
		if (!StringUtils.isEmpty(orderNo)) {
			orderInfoService.orderPay(orderNo);
		}
		channel.basicAck(message.getMessageProperties().getDeliveryTag(),
				false);
	}
}
