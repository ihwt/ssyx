package com.ssyx.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.auth.AuthThreadLocal;
import com.ssyx.model.order.OrderInfo;
import com.ssyx.result.Result;
import com.ssyx.service.OrderInfoService;
import com.ssyx.vo.order.OrderConfirmVo;
import com.ssyx.vo.order.OrderSubmitVo;
import com.ssyx.vo.order.OrderUserQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @program: ssyx-parent
 * @className: OrderApiController
 * @description: 订单管理类
 * @data: 2024/3/20 19:52
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "订单管理")
@RestController("OrderApiController")
@RequestMapping("api/order")
public class OrderApiController {
	
	@Resource
	private OrderInfoService orderService;
	
	@ApiOperation("确认订单")
	@GetMapping("auth/confirmOrder")
	public Result<OrderConfirmVo> confirm() {
		log.info("FeignClient(确认订单)");
		return Result.ok(orderService.confirmOrder());
	}
	
	@ApiOperation("生成订单")
	@PostMapping("auth/submitOrder")
	public Result<Long> submitOrder(@RequestBody OrderSubmitVo orderParamVo, HttpServletRequest request) {
		log.info("FeignClient(生成订单)：{}", orderParamVo);
		return Result.ok(orderService.submitOrder(orderParamVo));
	}
	
	@ApiOperation("获取订单详情")
	@GetMapping("auth/getOrderInfoById/{orderId}")
	public Result<OrderInfo> getOrderInfoById(@PathVariable("orderId") Long orderId) {
		log.info("FeignClient(获取订单详情)：{}", orderId);
		return Result.ok(orderService.getOrderInfoById(orderId));
	}
	
	@ApiOperation(value = "获取用户订单分页列表")
	@GetMapping("auth/findUserOrderPage/{page}/{limit}")
	public Result<IPage<OrderInfo>> findUserOrderPage(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			
			@ApiParam(name = "orderVo", value = "查询对象", required = false)
			OrderUserQueryVo orderUserQueryVo) {
		log.info("FeignClient(获取用户订单分页列表)：{},{}", page, limit);
		orderUserQueryVo.setUserId(AuthThreadLocal.getId());
		Page<OrderInfo> pageParam = new Page<>(page, limit);
		return Result.ok(orderService.findUserOrderPage(pageParam, orderUserQueryVo));
	}
	
}
