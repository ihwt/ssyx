package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.order.OrderInfo;
import com.ssyx.vo.order.OrderConfirmVo;
import com.ssyx.vo.order.OrderSubmitVo;
import com.ssyx.vo.order.OrderUserQueryVo;

public interface OrderInfoService extends IService<OrderInfo> {
	
	//确认订单
	OrderConfirmVo confirmOrder();
	
	//生成订单
	Long submitOrder(OrderSubmitVo orderParamVo);
	
	//订单详情
	OrderInfo getOrderInfoById(Long orderId);
	
	//根据orderNo查询订单信息
	OrderInfo getOrderInfoByOrderNo(String orderNo);
	
	//订单支付成功，更新订单状态，扣减库存
	void orderPay(String orderNo);
	
	//订单查询
	IPage<OrderInfo> findUserOrderPage(Page<OrderInfo> pageParam, OrderUserQueryVo orderUserQueryVo);
}
