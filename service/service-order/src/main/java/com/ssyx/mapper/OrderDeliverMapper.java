package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.order.OrderDeliver;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: OrderDeliverMapper
 * @description: 接口
 * @data: 2024/3/20 19:58
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface OrderDeliverMapper extends BaseMapper<OrderDeliver> {
}
