package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.order.OrderInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: OrderInfoMapper
 * @description: 接口
 * @data: 2024/3/20 19:56
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
}
