package com.ssyx.api;

import com.ssyx.activity.ActivityFeignClient;
import com.ssyx.auth.AuthThreadLocal;
import com.ssyx.model.order.CartInfo;
import com.ssyx.result.Result;
import com.ssyx.service.CartInfoService;
import com.ssyx.vo.order.OrderConfirmVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: CartController
 * @description: 购物车类
 * @data: 2024/3/19 14:08
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "购物车管理")
@RestController("CartApiController")
@RequestMapping("/api/cart")
public class CartApiController {
	
	@Resource
	private CartInfoService cartInfoService;
	@Resource
	private ActivityFeignClient activityFeignClient;
	
	/**
	 * 添加购物车
	 */
	@ApiOperation("添加购物车")
	@GetMapping("addToCart/{skuId}/{skuNum}")
	public Result<?> addToCart(@PathVariable("skuId") Long skuId,
	                           @PathVariable("skuNum") Integer skuNum) {
		log.info("FeignClient(添加购物车)：{}，{}", skuId, skuNum);
		cartInfoService.addToCart(skuId, AuthThreadLocal.getId(), skuNum);
		return Result.ok(null);
	}
	
	// 清空购物车
	@ApiOperation("修改购物车商品")
	@DeleteMapping("deleteCart/{skuId}")
	public Result<?> deleteCart(@PathVariable("skuId") Long skuId,
	                            HttpServletRequest request) {
		log.info("FeignClient(修改购物车商品)：{}", skuId);
		cartInfoService.deleteCart(skuId, AuthThreadLocal.getId());
		return Result.ok(null);
	}
	
	@ApiOperation(value = "清空购物车")
	@DeleteMapping("deleteAllCart")
	public Result<?> deleteAllCart(HttpServletRequest request) {
		log.info("FeignClient(清空购物)");
		cartInfoService.deleteAllCart(AuthThreadLocal.getId());
		return Result.ok(null);
	}
	
	@ApiOperation(value = "批量修改购物车商品")
	@PostMapping("batchDeleteCart")
	public Result<?> batchDeleteCart(@RequestBody List<Long> skuIdList, HttpServletRequest request) {
		log.info("FeignClient(批量修改购物车商品)：{}", skuIdList);
		cartInfoService.batchDeleteCart(skuIdList, AuthThreadLocal.getId());
		return Result.ok(null);
	}
	
	// 查询购物车列表
	@ApiOperation("查询购物车列表")
	@GetMapping("cartList")
	public Result<List<CartInfo>> cartList(HttpServletRequest request) {
		log.info("FeignClient(查询购物车列表)");
		return Result.ok(cartInfoService.getCartList(AuthThreadLocal.getId()));
	}
	
	// 查询带优惠卷的购物车
	@ApiOperation("查询带优惠卷的购物车")
	@GetMapping("activityCartList")
	public Result<OrderConfirmVo> activityCartList(HttpServletRequest request) {
		log.info("FeignClient(查询带优惠卷的购物车)");
		Long userId = AuthThreadLocal.getId();
		List<CartInfo> cartInfoList = cartInfoService.getCartList(userId);
		return Result.ok(activityFeignClient.findCartActivityAndCoupon(cartInfoList, userId));
	}
	
	/**
	 * 更新选中状态
	 */
	@ApiOperation("更新选中状态")
	@GetMapping("checkCart/{skuId}/{isChecked}")
	public Result<?> checkCart(@PathVariable(value = "skuId") Long skuId,
	                           @PathVariable(value = "isChecked") Integer isChecked, HttpServletRequest request) {
		log.info("FeignClient(更新选中状态)：{}，{}", skuId, isChecked);
		cartInfoService.checkCart(AuthThreadLocal.getId(), isChecked, skuId);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "选择购物车")
	@GetMapping("checkAllCart/{isChecked}")
	public Result<?> checkAllCart(@PathVariable(value = "isChecked") Integer isChecked, HttpServletRequest request) {
		log.info("FeignClient(更新选中状态)：{}", isChecked);
		cartInfoService.checkAllCart(AuthThreadLocal.getId(), isChecked);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "批量选择购物车")
	@PostMapping("batchCheckCart/{isChecked}")
	public Result<?> batchCheckCart(@RequestBody List<Long> skuIdList,
	                                @PathVariable(value = "isChecked") Integer isChecked, HttpServletRequest request) {
		log.info("FeignClient(更新选中状态)：{}，{}", skuIdList, isChecked);
		cartInfoService.batchCheckCart(skuIdList, AuthThreadLocal.getId(), isChecked);
		return Result.ok(null);
	}
	
	@ApiOperation("根据用户Id 查询购物车列表")
	@GetMapping("inner/getCartCheckedList/{userId}")
	public List<CartInfo> getCartCheckedList(@PathVariable("userId") Long userId) {
		log.info("FeignClient(根据用户Id 查询购物车列表)：{}", userId);
		return cartInfoService.getCartCheckedList(userId);
	}
}
