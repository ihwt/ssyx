package com.ssyx.receiver;

import com.rabbitmq.client.Channel;
import com.ssyx.constant.RabbitMQConstant;
import com.ssyx.service.CartInfoService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

@Component
public class CartReceiver {
	
	@Resource
	private CartInfoService cartInfoService;
	
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = RabbitMQConstant.QUEUE_DELETE_CART, durable = "true"),
			exchange = @Exchange(RabbitMQConstant.EXCHANGE_ORDER_DIRECT),
			key = {RabbitMQConstant.ROUTING_DELETE_CART}
	))
	public void deleteCart(Long userId, Message message, Channel channel) throws IOException {
		if (userId != null) {
			cartInfoService.deleteCartChecked(userId);
		}
		channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
	}
}
