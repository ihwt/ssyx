package com.ssyx.service;

import com.ssyx.model.order.CartInfo;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: cartInfoService
 * @description: 购物车类
 * @data: 2024/3/19 14:13
 * @author: ihu
 * @version: 1.0
 **/

public interface CartInfoService {
	void addToCart(Long skuId, Long id, Integer skuNum);
	
	void deleteCart(Long skuId, Long id);
	
	void deleteAllCart(Long id);
	
	void batchDeleteCart(List<Long> skuIdList, Long id);
	
	List<CartInfo> getCartList(Long id);
	
	void checkCart(Long id, Integer isChecked, Long skuId);
	
	void checkAllCart(Long id, Integer isChecked);
	
	void batchCheckCart(List<Long> skuIdList, Long id, Integer isChecked);
	
	List<CartInfo> getCartCheckedList(Long userId);
	
	void deleteCartChecked(Long userId);
}
