package com.ssyx.api;

import com.ssyx.model.search.SkuEs;
import com.ssyx.result.Result;
import com.ssyx.service.SkuService;
import com.ssyx.vo.search.SkuEsQueryVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuApiController
 * @description: 商品搜索类
 * @data: 2024/3/15 14:27
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@RestController("SkuApiController")
@RequestMapping("api/search/sku")
public class SkuApiController {
	
	@Resource
	private SkuService skuService;
	
	@ApiOperation(value = "上架商品")
	@GetMapping("inner/upperSku/{skuId}")
	public Result<?> upperSku(@PathVariable("skuId") Long skuId) {
		log.info("FeignClient(上架商品):{}", skuId);
		skuService.upperSku(skuId);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "下架商品")
	@GetMapping("inner/lowerSku/{skuId}")
	public Result<?> lowerSku(@PathVariable("skuId") Long skuId) {
		log.info("FeignClient(下架商品):{}", skuId);
		skuService.lowerSku(skuId);
		return Result.ok(null);
	}
	
	@ApiOperation(value = "获取爆品商品")
	@GetMapping("inner/findHotSkuList")
	public List<SkuEs> findHotSkuList() {
		log.info("FeignClient(获取爆品商品)");
		return skuService.findHotSkuList();
	}
	
	@ApiOperation(value = "更新商品incrHotScore")
	@GetMapping("inner/incrHotScore/{skuId}")
	public Boolean incrHotScore(@PathVariable("skuId") Long skuId) {
		log.info("FeignClient(更新商品incrHotScore):{}", skuId);
		skuService.incrHotScore(skuId);
		return true;
	}
	
	@ApiOperation(value = "搜索商品")
	@GetMapping("{page}/{limit}")
	public Result<Page<SkuEs>> list(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Integer page,
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Integer limit,
			@ApiParam(name = "searchParamVo", value = "查询对象", required = false)
			SkuEsQueryVo searchParamVo) {
		log.info("FeignClient(搜索商品):{}", searchParamVo);
		Pageable pageable = PageRequest.of(page - 1, limit);
		return Result.ok(skuService.search(pageable, searchParamVo));
	}
}
