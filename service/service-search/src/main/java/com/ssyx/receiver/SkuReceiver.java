package com.ssyx.receiver;

/**
 * @program: ssyx-parent
 * @className: SkuReceiver
 * @description: 接收消息类
 * @data: 2024/3/15 15:08
 * @author: ihu
 * @version: 1.0
 **/

import com.rabbitmq.client.Channel;
import com.ssyx.constant.RabbitMQConstant;
import com.ssyx.service.SkuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

@Slf4j
@Component
public class SkuReceiver {
	
	@Resource
	private SkuService skuService;
	
	/**
	 * 商品上架
	 */
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = RabbitMQConstant.QUEUE_GOODS_UPPER, durable = "true"),
			exchange = @Exchange(RabbitMQConstant.EXCHANGE_GOODS_DIRECT),
			key = {RabbitMQConstant.ROUTING_GOODS_UPPER}
	))
	public void upperSku(Long skuId, Message message, Channel channel) throws IOException {
		if (skuId != null) {
			skuService.upperSku(skuId);
		}
		// 第一个参数：表示收到的消息的标号 第二个参数：如果为true表示可以签收多个消息
		channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
	}
	
	/**
	 * 商品下架
	 */
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = RabbitMQConstant.QUEUE_GOODS_LOWER, durable = "true"),
			exchange = @Exchange(RabbitMQConstant.EXCHANGE_GOODS_DIRECT),
			key = {RabbitMQConstant.ROUTING_GOODS_LOWER}
	))
	public void lowerSku(Long skuId, Message message, Channel channel) throws IOException {
		if (skuId != null) {
			skuService.lowerSku(skuId);
		}
		channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
	}
}
