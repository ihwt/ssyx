package com.ssyx.repository;


import com.ssyx.model.search.SkuEs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: ssyx-parent
 * @className: SkuRepository
 * @description: 接口
 * @data: 2024/3/15 14:34
 * @author: ihu
 * @version: 1.0
 **/

@Repository
public interface SkuRepository extends ElasticsearchRepository<SkuEs, Long> {
	Page<SkuEs> findByOrderByHotScoreDesc(Pageable page);
	
	Page<SkuEs> findByCategoryIdAndWareId(Long categoryId, Long wareId, Pageable page);
	
	Page<SkuEs> findByKeywordAndWareId(String keyword, Long wareId, Pageable page);
}