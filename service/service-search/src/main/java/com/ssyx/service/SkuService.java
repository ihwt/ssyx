package com.ssyx.service;

import com.ssyx.model.search.SkuEs;
import com.ssyx.vo.search.SkuEsQueryVo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SkuService
 * @description: 接口
 * @data: 2024/3/15 14:28
 * @author: ihu
 * @version: 1.0
 **/

public interface SkuService {
	void upperSku(Long skuId);
	
	void lowerSku(Long skuId);
	
	List<SkuEs> findHotSkuList();
	
	Page<SkuEs> search(Pageable pageable, SkuEsQueryVo skuEsQueryVo);
	
	void incrHotScore(Long skuId);
}
