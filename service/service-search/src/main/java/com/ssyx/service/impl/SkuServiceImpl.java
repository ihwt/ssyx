package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.ssyx.activity.ActivityFeignClient;
import com.ssyx.auth.AuthThreadLocal;
import com.ssyx.constant.GlobalConstant;
import com.ssyx.enums.SkuType;
import com.ssyx.exception.SsyxException;
import com.ssyx.model.product.Category;
import com.ssyx.model.product.SkuInfo;
import com.ssyx.model.search.SkuEs;
import com.ssyx.product.ProductFeignClient;
import com.ssyx.repository.SkuRepository;
import com.ssyx.result.ResultCodeEnum;
import com.ssyx.service.SkuService;
import com.ssyx.vo.search.SkuEsQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @program: ssyx-parent
 * @className: SkuServiceImpl
 * @description: 类
 * @data: 2024/3/15 14:32
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class SkuServiceImpl implements SkuService {
	
	@Resource
	private ProductFeignClient productFeignClient;
	@Resource
	private SkuRepository skuEsRepository;
	@Resource
	private ActivityFeignClient activityFeignClient;
	@Resource
	private RestHighLevelClient restHighLevelClient;
	@Resource
	private RedisTemplate<String, Object> redisTemplate;
	
	/**
	 * 上架商品列表
	 */
	@Override
	public void upperSku(Long skuId) {
		//查询sku信息
		SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
		if (skuInfo == null) {
			log.error("skuId:{}商品不存在", skuId);
			throw new SsyxException(ResultCodeEnum.INFORMATION_NOT_EXIST);
		}
		// 查询分类
		Category category = productFeignClient.getCategory(skuInfo.getCategoryId());
		if (category == null) {
			log.error("skuId:{}分类不存在", skuId);
			throw new SsyxException(ResultCodeEnum.INFORMATION_NOT_EXIST);
		}
		SkuEs skuEs =
				SkuEs.builder().categoryId(category.getId()).categoryName(category.getName()).id(skuInfo.getId()).keyword(skuInfo.getSkuName() + "," + category.getName()).wareId(skuInfo.getWareId()).isNewPerson(skuInfo.getIsNewPerson()).imgUrl(skuInfo.getImgUrl()).title(skuInfo.getSkuName()).build();
		if (skuInfo.getSkuType().equals(SkuType.COMMON.getCode())) {
			skuEs.setSkuType(GlobalConstant.INTEGER_ZERO);
			skuEs.setPrice(skuInfo.getPrice().doubleValue());
			skuEs.setStock(skuInfo.getStock());
			skuEs.setSale(skuInfo.getSale());
			skuEs.setPerLimit(skuInfo.getPerLimit());
		}
//		else {
//			//TODO 待完善-秒杀商品
//
//		}
		SkuEs save = skuEsRepository.save(skuEs);
		log.info("添加es后完整数据：SkuES:{}", save);
	}
	
	/**
	 * 下架商品列表
	 */
	@Override
	public void lowerSku(Long skuId) {
		skuEsRepository.deleteById(skuId);
	}
	
	@Override
	public List<SkuEs> findHotSkuList() {
		Pageable pageable = PageRequest.of(GlobalConstant.INTEGER_ZERO, 10);
		return skuEsRepository.findByOrderByHotScoreDesc(pageable).getContent();
	}
	
	@Override
	public Page<SkuEs> search(Pageable pageable, SkuEsQueryVo skuEsQueryVo) {
		skuEsQueryVo.setWareId(AuthThreadLocal.getWare());
		Page<SkuEs> page = null;
		if (StringUtils.isEmpty(skuEsQueryVo.getKeyword())) {
			page = skuEsRepository.findByCategoryIdAndWareId(skuEsQueryVo.getCategoryId(), skuEsQueryVo.getWareId(),
					pageable);
		} else {
			page = skuEsRepository.findByKeywordAndWareId(skuEsQueryVo.getKeyword(), skuEsQueryVo.getWareId(), pageable);
		}
		List<SkuEs> skuEsList = page.getContent();
		//获取sku对应的促销活动标签
		if (!CollectionUtils.isEmpty(skuEsList)) {
			List<Long> skuIdList = skuEsList.stream().map(SkuEs::getId).collect(Collectors.toList());
			Map<Long, List<String>> skuIdToRuleListMap = activityFeignClient.findActivity(skuIdList);
			if (null != skuIdToRuleListMap) {
				skuEsList.forEach(skuEs -> {
					skuEs.setRuleList(skuIdToRuleListMap.get(skuEs.getId()));
				});
			}
		}
		return page;
	}
	
	@Override
	public void incrHotScore(Long skuId) {
		// 定义key
		String hotKey = "hotScore";
		// 保存数据
		Double hotScore = redisTemplate.opsForZSet().incrementScore(hotKey, "skuId:" + skuId, 1);
		if (hotScore % 10 == GlobalConstant.INTEGER_ZERO) {
			// 更新es
			Optional<SkuEs> optional = skuEsRepository.findById(skuId);
			SkuEs skuEs = optional.get();
			skuEs.setHotScore(Math.round(hotScore));
			skuEsRepository.save(skuEs);
		}
	}
}
