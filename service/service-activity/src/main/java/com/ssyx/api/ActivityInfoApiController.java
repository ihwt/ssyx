package com.ssyx.api;

import com.ssyx.model.activity.CouponInfo;
import com.ssyx.model.order.CartInfo;
import com.ssyx.service.ActivityInfoService;
import com.ssyx.service.CouponInfoService;
import com.ssyx.vo.order.CartInfoVo;
import com.ssyx.vo.order.OrderConfirmVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Slf4j
@Api(tags = "优惠")
@RestController("ActivityInfoApiController")
@RequestMapping("/api/activity")
public class ActivityInfoApiController {
	
	@Resource
	private ActivityInfoService activityInfoService;
	@Resource
	private CouponInfoService couponInfoService;
	
	//获取购物车里面满足条件优惠卷和活动的信息
	@ApiOperation("根据购物车获取促销信息")
	@PostMapping("inner/findCartActivityAndCoupon/{userId}")
	public OrderConfirmVo findCartActivityAndCoupon(@RequestBody List<CartInfo> cartInfoList,
	                                                @PathVariable("userId") Long userId) {
		log.info("FeignClient(根据购物车获取促销信息):{},{}", userId, cartInfoList);
		return activityInfoService.findCartActivityAndCoupon(cartInfoList, userId);
	}
	
	@ApiOperation("根据skuId列表获取促销信息")
	@PostMapping("inner/findActivity")
	public Map<Long, List<String>> findActivity(@RequestBody List<Long> skuIdList) {
		log.info("FeignClient(根据skuId列表获取促销信息):{}", skuIdList);
		return activityInfoService.findActivity(skuIdList);
	}
	
	@ApiOperation("根据skuID获取营销数据和优惠卷")
	@GetMapping("inner/findActivityAndCoupon/{skuId}/{userId}")
	public Map<String, Object> findActivityAndCoupon(@PathVariable Long skuId,
	                                                 @PathVariable Long userId) {
		log.info("FeignClient(根据skuID获取营销数据和优惠卷):{},{}", skuId, userId);
		return activityInfoService.findActivityAndCoupon(skuId, userId);
	}
	
	//获取购物车对应规则数据
	@ApiOperation("获取购物车对应优惠卷")
	@PostMapping("inner/findCartActivityList")
	public List<CartInfoVo> findCartActivityList(@RequestBody List<CartInfo> cartInfoList) {
		log.info("FeignClient(获取购物车对应优惠卷):{}", cartInfoList);
		return activityInfoService.findCartActivityList(cartInfoList);
	}
	
	//获取购物车对应优惠卷
	@ApiOperation("获取购物车对应优惠卷")
	@PostMapping("inner/findRangeSkuIdList/{couponId}")
	public CouponInfo findRangeSkuIdList(@RequestBody List<CartInfo> cartInfoList,
	                                     @PathVariable("couponId") Long couponId) {
		log.info("FeignClient(获取购物车对应优惠卷):{},{}", cartInfoList, couponId);
		return couponInfoService.findRangeSkuIdList(cartInfoList, couponId);
	}
	
	//更新优惠卷使用状态
	@ApiOperation("更新优惠券使用状态")
	@GetMapping("inner/updateCouponInfoUseStatus/{couponId}/{userId}/{orderId}")
	public Boolean updateCouponInfoUseStatus(@PathVariable("couponId") Long couponId,
	                                         @PathVariable("userId") Long userId,
	                                         @PathVariable("orderId") Long orderId) {
		log.info("FeignClient(更新优惠券使用状态):{},{},{}", couponId, userId, orderId);
		couponInfoService.updateCouponInfoUseStatus(couponId, userId, orderId);
		return true;
	}
}
