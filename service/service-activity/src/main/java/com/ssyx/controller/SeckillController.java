package com.ssyx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.model.activity.Seckill;
import com.ssyx.result.Result;
import com.ssyx.service.SeckillService;
import com.ssyx.vo.activity.SeckillQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SeckillController
 * @description: Seckill管理类
 * @data: 2024/3/15 20:56
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "Seckill管理")
@RestController("SeckillController")
@RequestMapping(value = "/admin/activity/seckill")
@SuppressWarnings({"unchecked", "rawtypes"})
public class SeckillController {
	
	@Resource
	private SeckillService seckillService;
	
	@ApiOperation("获取分页列表")
	@GetMapping("{page}/{limit}")
	public Result<IPage<Seckill>> index(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			
			@ApiParam(name = "seckillQueryVo", value = "查询对象", required = false)
			SeckillQueryVo seckillQueryVo) {
		log.info("获取角色分页列表：page:{},limit:{},seckillQueryVo:{}", page, limit, seckillQueryVo);
		Page<Seckill> pageParam = new Page<>(page, limit);
		return Result.ok(seckillService.selectPage(pageParam, seckillQueryVo));
	}
	
	@ApiOperation("根据id获取秒杀信息")
	@GetMapping("get/{id}")
	public Result<Seckill> get(@PathVariable Long id) {
		log.info("根据id获取秒杀信息：id:{}", id);
		return Result.ok(seckillService.getById(id));
	}
	
	@ApiOperation("新增秒杀信息")
	@PostMapping("save")
	public Result<?> save(@RequestBody Seckill seckill) {
		log.info("新增秒杀信息：seckill:{}", seckill);
		seckillService.save(seckill);
		return Result.ok(null);
	}
	
	@ApiOperation("修改秒杀信息")
	@PutMapping("update")
	public Result<?> updateById(@RequestBody Seckill seckill) {
		log.info("修改秒杀信息：seckill:{}", seckill);
		seckillService.updateById(seckill);
		return Result.ok(null);
	}
	
	@ApiOperation("删除秒杀信息")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("删除秒杀信息：id:{}", id);
		seckillService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation("批量删除秒杀信息")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("批量删除秒杀信息：idList:{}", idList);
		seckillService.removeByIds(idList);
		return Result.ok(null);
	}
	
	@ApiOperation("更新状态")
	@PostMapping("updateStatus/{id}/{status}")
	public Result<?> updateStatus(@PathVariable Long id, @PathVariable Integer status) {
		log.info("更新状态：id:{},status:{}", id, status);
		seckillService.update(Wrappers.<Seckill>lambdaUpdate().set(Seckill::getStatus, status).eq(Seckill::getId, id));
		return Result.ok(null);
	}
}
