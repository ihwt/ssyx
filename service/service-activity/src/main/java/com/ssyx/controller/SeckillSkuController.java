package com.ssyx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.model.activity.SeckillSku;
import com.ssyx.result.Result;
import com.ssyx.service.SeckillSkuService;
import com.ssyx.vo.activity.SeckillSkuQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SeckillSkuController
 * @description: SeckillSku管理类
 * @data: 2024/3/15 21:07
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "SeckillSku管理")
@RestController("SeckillSkuController")
@RequestMapping(value = "/admin/activity/seckillSku")
@SuppressWarnings({"unchecked", "rawtypes"})
public class SeckillSkuController {
	
	@Resource
	private SeckillSkuService seckillSkuService;
	
	@ApiOperation(value = "获取分页列表")
	@GetMapping("{page}/{limit}")
	public Result<IPage<SeckillSku>> index(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			
			@ApiParam(name = "seckillSkuQueryVo", value = "查询对象", required = false)
			SeckillSkuQueryVo seckillSkuQueryVo) {
		log.info("获取角色分页列表：page:{},limit:{},seckillSkuQueryVo:{}", page, limit, seckillSkuQueryVo);
		Page<SeckillSku> pageParam = new Page<>(page, limit);
		return Result.ok(seckillSkuService.selectPage(pageParam, seckillSkuQueryVo));
	}
	
	@ApiOperation("获取秒杀商品")
	@GetMapping("get/{id}")
	public Result<SeckillSku> get(@PathVariable Long id) {
		log.info("获取秒杀商品：id:{}", id);
		return Result.ok(seckillSkuService.getById(id));
	}
	
	@ApiOperation("新增秒杀商品")
	@PostMapping("save")
	public Result<?> save(@RequestBody List<SeckillSku> seckillSkuList) {
		log.info("新增秒杀商品：seckillSkuList:{}", seckillSkuList);
		seckillSkuService.saveBatch(seckillSkuList);
		return Result.ok(null);
	}
	
	@ApiOperation("修改秒杀商品")
	@PutMapping("update")
	public Result<?> updateById(@RequestBody SeckillSku seckillSku) {
		log.info("修改秒杀商品：seckillSku:{}", seckillSku);
		seckillSkuService.updateById(seckillSku);
		return Result.ok(null);
	}
	
	@ApiOperation("删除秒杀商品")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("删除秒杀商品：id:{}", id);
		seckillSkuService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation("批量删除秒杀商品")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("批量删除秒杀商品：idList:{}", idList);
		seckillSkuService.removeByIds(idList);
		return Result.ok(null);
	}
}
