package com.ssyx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.model.activity.CouponInfo;
import com.ssyx.result.Result;
import com.ssyx.service.CouponInfoService;
import com.ssyx.vo.activity.CouponRuleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: CouponInfoController
 * @description: 优惠卷管理类
 * @data: 2024/3/15 20:26
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "优惠卷管理")
@RestController("CouponInfoController")
@RequestMapping("/admin/activity/couponInfo")
public class CouponInfoController {
	
	@Resource
	private CouponInfoService couponInfoService;
	
	@ApiOperation("获取分页列表")
	@GetMapping("{page}/{limit}")
	public Result<IPage<CouponInfo>> index(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit
	) {
		log.info("获取角色分页列表：page:{},limit:{}", page, limit);
		Page<CouponInfo> pageParam = new Page<>(page, limit);
		return Result.ok(couponInfoService.selectPage(pageParam));
	}
	
	@ApiOperation("获取优惠券")
	@GetMapping("get/{id}")
	public Result<CouponInfo> get(@PathVariable String id) {
		log.info("获取优惠券：id:{}", id);
		return Result.ok(couponInfoService.getCouponInfo(id));
	}
	
	@ApiOperation("新增优惠券")
	@PostMapping("save")
	public Result<?> save(@RequestBody CouponInfo couponInfo) {
		log.info("新增优惠券：{}", couponInfo);
		couponInfoService.save(couponInfo);
		return Result.ok(null);
	}
	
	@ApiOperation("修改优惠券")
	@PutMapping("update")
	public Result<?> updateById(@RequestBody CouponInfo couponInfo) {
		log.info("修改优惠券：{}", couponInfo);
		couponInfoService.updateById(couponInfo);
		return Result.ok(null);
	}
	
	@ApiOperation("删除优惠券")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("删除优惠券：id:{}", id);
		couponInfoService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation("根据id列表删除优惠券")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("删除优惠券：idList:{}", idList);
		couponInfoService.removeByIds(idList);
		return Result.ok(null);
	}
	
	@ApiOperation("获取优惠券信息")
	@GetMapping("findCouponRuleList/{id}")
	public Result<Map<String, Object>> findActivityRuleList(@PathVariable Long id) {
		log.info("获取优惠券信息：id:{}", id);
		return Result.ok(couponInfoService.findCouponRuleList(id));
	}
	
	@ApiOperation("新增活动")
	@PostMapping("saveCouponRule")
	public Result<?> saveCouponRule(@RequestBody CouponRuleVo couponRuleVo) {
		log.info("新增优惠券信息：couponRuleVo:{}", couponRuleVo);
		couponInfoService.saveCouponRule(couponRuleVo);
		return Result.ok(null);
	}
	
	@ApiOperation("根据关键字获取sku列表")
	@GetMapping("findCouponByKeyword/{keyword}")
	public Result<List<CouponInfo>> findCouponByKeyword(@PathVariable("keyword") String keyword) {
		log.info("根据关键字获取sku列表：keyword:{}", keyword);
		return Result.ok(couponInfoService.list(Wrappers.<CouponInfo>lambdaQuery().like(CouponInfo::getCouponName,
				keyword)));
	}
}
