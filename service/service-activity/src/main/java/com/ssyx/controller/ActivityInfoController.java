package com.ssyx.controller;

/**
 * @program: ssyx-parent
 * @className: ActivityInfoController
 * @description: 活动类
 * @data: 2024/3/15 19:51
 * @author: ihu
 * @version: 1.0
 **/

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.constant.GlobalConstant;
import com.ssyx.enums.ActivityType;
import com.ssyx.model.activity.ActivityInfo;
import com.ssyx.model.product.SkuInfo;
import com.ssyx.result.Result;
import com.ssyx.service.ActivityInfoService;
import com.ssyx.vo.activity.ActivityRuleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Api(tags = "活动管理")
@RestController("ActivityInfoController")
@RequestMapping("/admin/activity/activityInfo")
public class ActivityInfoController {
	
	@Resource
	private ActivityInfoService activityInfoService;
	
	@ApiOperation("获取分页列表")
	@GetMapping("{page}/{limit}")
	public Result<IPage<ActivityInfo>> index(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit) {
		log.info("获取角色分页列表：page:{},limit:{}", page, limit);
		Page<ActivityInfo> pageParam = new Page<>(page, limit);
		return Result.ok(activityInfoService.selectPage(pageParam));
	}
	
	@ApiOperation("获取活动")
	@GetMapping("get/{id}")
	public Result<ActivityInfo> get(@PathVariable Long id) {
		log.info("获取活动：id:{}", id);
		ActivityInfo activityInfo = activityInfoService.getById(id);
		if (activityInfo.getActivityType().equals(GlobalConstant.INTEGER_ONE)) {
			activityInfo.setActivityTypeString(ActivityType.FULL_REDUCTION.getComment());
		} else {
			activityInfo.setActivityTypeString(ActivityType.FULL_DISCOUNT.getComment());
		}
		
		return Result.ok(activityInfo);
	}
	
	@ApiOperation("新增活动")
	@PostMapping("save")
	public Result<?> save(@RequestBody ActivityInfo activityInfo) {
		log.info("新增活动：activityInfo:{}", activityInfo);
		activityInfo.setCreateTime(new Date());
		activityInfoService.save(activityInfo);
		return Result.ok(null);
	}
	
	@ApiOperation("修改活动")
	@PutMapping("update")
	public Result<?> updateById(@RequestBody ActivityInfo activityInfo) {
		log.info("修改活动：activityInfo:{}", activityInfo);
		activityInfo.setUpdateTime(new Date());
		activityInfoService.updateById(activityInfo);
		return Result.ok(null);
	}
	
	@ApiOperation("删除活动")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("删除活动：id:{}", id);
		activityInfoService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation("批量删除活动")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<String> idList) {
		log.info("批量删除活动：idList:{}", idList);
		activityInfoService.removeByIds(idList);
		return Result.ok(null);
	}
	
	@ApiOperation("获取活动规则")
	@GetMapping("findActivityRuleList/{id}")
	public Result<Map<String, Object>> findActivityRuleList(@PathVariable Long id) {
		log.info("获取活动规则：id:{}", id);
		return Result.ok(activityInfoService.findActivityRuleList(id));
	}
	
	@ApiOperation(value = "新增活动规则")
	@PostMapping("saveActivityRule")
	public Result<?> saveActivityRule(@RequestBody ActivityRuleVo activityRuleVo) {
		log.info("新增活动规则：activityRuleVo:{}", activityRuleVo);
		activityInfoService.saveActivityRule(activityRuleVo);
		return Result.ok(null);
	}
	
	@ApiOperation("根据关键字获取sku列表")
	@GetMapping("findSkuInfoByKeyword/{keyword}")
	public Result<List<SkuInfo>> findSkuInfoByKeyword(@PathVariable("keyword") String keyword) {
		return Result.ok(activityInfoService.findSkuInfoByKeyword(keyword));
	}
}
