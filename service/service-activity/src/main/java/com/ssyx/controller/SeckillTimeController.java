package com.ssyx.controller;

/**
 * @program: ssyx-parent
 * @className: SeckillTimeController
 * @description: 秒杀场次管理类
 * @data: 2024/3/15 21:17
 * @author: ihu
 * @version: 1.0
 **/

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ssyx.model.activity.SeckillTime;
import com.ssyx.result.Result;
import com.ssyx.service.SeckillTimeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Api(tags = "秒杀场次管理")
@RestController("SeckillTimeController")
@RequestMapping(value = "/admin/activity/seckillTime")
@SuppressWarnings({"unchecked", "rawtypes"})
public class SeckillTimeController {
	
	@Resource
	private SeckillTimeService seckillTimeService;
	
	@ApiOperation("获取全部场次")
	@GetMapping()
	public Result<List<SeckillTime>> index() {
		log.info("获取全部场次");
		return Result.ok(seckillTimeService.list());
	}
	
	@ApiOperation("根据id获取场次")
	@GetMapping("get/{id}")
	public Result<SeckillTime> get(@PathVariable Long id) {
		log.info("根据id获取场次：id:{}", id);
		return Result.ok(seckillTimeService.getById(id));
	}
	
	@ApiOperation("新增场次")
	@PostMapping("save")
	public Result<?> save(@RequestBody SeckillTime seckillSession) {
		log.info("新增场次：{}", seckillSession);
		seckillTimeService.save(seckillSession);
		return Result.ok(null);
	}
	
	@ApiOperation("修改场次")
	@PutMapping("update")
	public Result<?> updateById(@RequestBody SeckillTime seckillSession) {
		log.info("修改场次：{}", seckillSession);
		seckillTimeService.updateById(seckillSession);
		return Result.ok(null);
	}
	
	@ApiOperation("删除场次")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("删除场次：id:{}", id);
		seckillTimeService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation("批量删除场次")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("批量删除场次：idList:{}", idList);
		seckillTimeService.removeByIds(idList);
		return Result.ok(null);
	}
	
	@ApiOperation("更新状态")
	@PostMapping("updateStatus/{id}/{status}")
	public Result<?> updateStatus(@PathVariable Long id, @PathVariable Integer status) {
		seckillTimeService.update(Wrappers.<SeckillTime>lambdaUpdate().set(SeckillTime::getStatus, status).eq(SeckillTime::getId, id));
		return Result.ok(null);
	}
}
