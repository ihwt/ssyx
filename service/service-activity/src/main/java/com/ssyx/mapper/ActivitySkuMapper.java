package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.activity.ActivitySku;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: ActivitySkuMapper
 * @description: 接口
 * @data: 2024/3/15 20:09
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface ActivitySkuMapper extends BaseMapper<ActivitySku> {
}
