package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.activity.CouponUse;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: CouponUseMapper
 * @description: 接口
 * @data: 2024/3/15 20:34
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface CouponUseMapper extends BaseMapper<CouponUse> {
}
