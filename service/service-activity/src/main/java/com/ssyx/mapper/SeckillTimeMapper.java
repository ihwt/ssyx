package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.activity.SeckillTime;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: SeckillTimeMapper
 * @description: 接口
 * @data: 2024/3/15 21:25
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface SeckillTimeMapper extends BaseMapper<SeckillTime> {
}
