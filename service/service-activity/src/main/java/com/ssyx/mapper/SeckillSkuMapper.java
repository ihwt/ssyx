package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.activity.SeckillSku;
import com.ssyx.vo.activity.SeckillSkuVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SeckillSkuMapper
 * @description: 接口
 * @data: 2024/3/15 21:15
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface SeckillSkuMapper extends BaseMapper<SeckillSku> {
	List<SeckillSkuVo> findSeckillSkuListByDate(String date);
}
