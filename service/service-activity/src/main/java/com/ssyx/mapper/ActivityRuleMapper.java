package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.activity.ActivityRule;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: ActivityRuleMapper
 * @description: 接口
 * @data: 2024/3/15 20:05
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface ActivityRuleMapper extends BaseMapper<ActivityRule> {
}
