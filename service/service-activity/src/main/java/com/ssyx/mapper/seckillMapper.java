package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.activity.Seckill;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: seckillMapper
 * @description: 接口
 * @data: 2024/3/15 21:02
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface seckillMapper extends BaseMapper<Seckill> {
}
