package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.activity.CouponRange;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: CouponRangeMapper
 * @description: 接口
 * @data: 2024/3/15 20:34
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface CouponRangeMapper extends BaseMapper<CouponRange> {
}
