package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.activity.ActivityInfo;
import com.ssyx.model.activity.ActivityRule;
import com.ssyx.model.activity.ActivitySku;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: ActivityInfoMapper
 * @description: 接口
 * @data: 2024/3/15 19:53
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface ActivityInfoMapper extends BaseMapper<ActivityInfo> {
	List<Long> selectExistSkuIdList(@Param("skuIdList") List<Long> skuIdList);
	
	List<ActivityRule> selectActivityRuleList(@Param("skuId") Long skuId);
	
	
	// 如果之前参加过，活动正在进行中，排除商品
	List<Long> selectSkuIdListExist(@Param("skuIdList") List<Long> skuIdList);
	
	//根据skuId进行查询，查询sku对应活动里面规则列表
	List<ActivityRule> findActivityRule(@Param("skuId") Long skuId);
	
	//根据所有skuId列表获取参与活动
	List<ActivitySku> selectCartActivity(@Param("skuIdList") List<Long> skuIdList);
}
