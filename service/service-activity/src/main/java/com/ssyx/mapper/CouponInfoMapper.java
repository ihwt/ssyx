package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.activity.CouponInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: CouponInfoMapper
 * @description: 接口
 * @data: 2024/3/15 20:33
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface CouponInfoMapper extends BaseMapper<CouponInfo> {
	// 根据skuId+userId查询优惠卷信息
	List<CouponInfo> selectCouponInfoList(@Param("skuId") Long id,
	                                      @Param("categoryId") Long categoryId,
	                                      @Param("userId") Long userId);
	
	// 根据userId获取用户全部优惠卷
	List<CouponInfo> selectCartCouponInfoList(@Param("userId") Long userId);
}
