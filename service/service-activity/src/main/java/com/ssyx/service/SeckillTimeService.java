package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.activity.SeckillTime;

import java.util.List;
import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: SeckillTimeService
 * @description: 类
 * @data: 2024/3/15 21:18
 * @author: ihu
 * @version: 1.0
 **/

public interface SeckillTimeService extends IService<SeckillTime> {
	
	/**
	 * 查询秒杀时间段列表放入缓存
	 */
	void saveSeckillTimeListToCache();
	
	/**
	 * 从缓存中查询秒杀时间段
	 */
	List<SeckillTime> findAllSeckillTimeListFromCache();
	
	/**
	 * 获取用户端首页秒杀数据
	 */
	Map<String, Object> findHomeData();
}
