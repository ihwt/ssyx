package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.activity.CouponInfo;
import com.ssyx.model.order.CartInfo;
import com.ssyx.vo.activity.CouponRuleVo;

import java.util.List;
import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: CouponInfoService
 * @description: 接口
 * @data: 2024/3/15 20:27
 * @author: ihu
 * @version: 1.0
 **/

public interface CouponInfoService extends IService<CouponInfo> {
	
	//优惠卷分页查询
	IPage<CouponInfo> selectPage(Page<CouponInfo> pageParam);
	
	//根据id获取优惠券
	CouponInfo getCouponInfo(String id);
	
	//根据优惠卷id获取优惠券规则列表
	Map<String, Object> findCouponRuleList(Long couponId);
	
	//新增优惠券规则
	void saveCouponRule(CouponRuleVo couponRuleVo);
	
	CouponInfo findRangeSkuIdList(List<CartInfo> cartInfoList, Long couponId);
	
	void updateCouponInfoUseStatus(Long couponId, Long userId, Long orderId);
	
	List<CouponInfo> findCartCouponInfo(List<CartInfo> cartInfoList, Long userId);
	
	List<CouponInfo> findCouponInfoList(Long skuId, Long userId);
}
