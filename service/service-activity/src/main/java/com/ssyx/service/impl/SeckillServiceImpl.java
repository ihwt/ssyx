package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.mapper.seckillMapper;
import com.ssyx.model.activity.Seckill;
import com.ssyx.service.SeckillService;
import com.ssyx.vo.activity.SeckillQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @program: ssyx-parent
 * @className: SeckillServiceImpl
 * @description: 类
 * @data: 2024/3/15 21:01
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
@SuppressWarnings({"unchecked", "rawtypes"})
public class SeckillServiceImpl extends ServiceImpl<seckillMapper, Seckill> implements SeckillService {
	
	@Override
	public IPage<Seckill> selectPage(Page<Seckill> pageParam, SeckillQueryVo seckillQueryVo) {
		Integer status = seckillQueryVo.getStatus();
		String title = seckillQueryVo.getTitle();
		return baseMapper.selectPage(pageParam, Wrappers.<Seckill>lambdaQuery().eq(!StringUtils.isEmpty(status),
				Seckill::getStatus, status).like(!StringUtils.isEmpty(title),
				Seckill::getTitle, title));
	}
	
}
