package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.activity.SeckillSku;
import com.ssyx.vo.activity.SeckillSkuQueryVo;
import com.ssyx.vo.activity.SeckillSkuVo;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: SeckillSkuService
 * @description: 接口
 * @data: 2024/3/15 21:07
 * @author: ihu
 * @version: 1.0
 **/

public interface SeckillSkuService extends IService<SeckillSku> {
	IPage<SeckillSku> selectPage(Page<SeckillSku> pageParam, SeckillSkuQueryVo seckillSkuQueryVo);
	
	//	void saveSeckillSku(List<SeckillSku> seckillSkuList);
//
	List<SeckillSkuVo> findSeckillSkuListFromCache(String timeName);
	
	/**
	 * 根据skuId获取秒杀sku信息
	 */
//	SeckillSkuVo getSeckillSkuVo(Long skuId);
//
//	Boolean checkAndMinusStock(List<SkuStockLockVo> skuStockLockVoList, String orderToken);
//
//	void rollBackStock(String orderNo);
}
