package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.activity.Seckill;
import com.ssyx.vo.activity.SeckillQueryVo;

/**
 * @program: ssyx-parent
 * @className: SeckillService
 * @description: 接口
 * @data: 2024/3/15 20:57
 * @author: ihu
 * @version: 1.0
 **/

public interface SeckillService extends IService<Seckill> {
	IPage<Seckill> selectPage(Page<Seckill> pageParam, SeckillQueryVo seckillQueryVo);
}
