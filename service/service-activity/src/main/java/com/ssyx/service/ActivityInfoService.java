package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.activity.ActivityInfo;
import com.ssyx.model.activity.ActivityRule;
import com.ssyx.model.order.CartInfo;
import com.ssyx.model.product.SkuInfo;
import com.ssyx.vo.activity.ActivityRuleVo;
import com.ssyx.vo.order.CartInfoVo;
import com.ssyx.vo.order.OrderConfirmVo;

import java.util.List;
import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: ActivityInfoService
 * @description: 接口
 * @data: 2024/3/15 19:52
 * @author: ihu
 * @version: 1.0
 **/

public interface ActivityInfoService extends IService<ActivityInfo> {
	IPage<ActivityInfo> selectPage(Page<ActivityInfo> pageParam);
	
	/**
	 * 获取活动规则id
	 */
	Map<String, Object> findActivityRuleList(Long activityId);
	
	//保存活动规则信息
	void saveActivityRule(ActivityRuleVo activityRuleVo);
	
	//根据关键字获取sku信息列表
	List<SkuInfo> findSkuInfoByKeyword(String keyword);
	
	/**
	 * 根据skuId获取促销规则信息
	 */
	List<ActivityRule> findActivityRule(Long skuId);
	
	OrderConfirmVo findCartActivityAndCoupon(List<CartInfo> cartInfoList, Long userId);
	
	Map<Long, List<String>> findActivity(List<Long> skuIdList);
	
	Map<String, Object> findActivityAndCoupon(Long skuId, Long userId);
	
	List<CartInfoVo> findCartActivityList(List<CartInfo> cartInfoList);
}
