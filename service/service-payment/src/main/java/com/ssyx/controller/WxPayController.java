package com.ssyx.controller;

import com.ssyx.enums.PaymentType;
import com.ssyx.result.Result;
import com.ssyx.service.WeixinService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: WxPayController
 * @description: 微信支付类
 * @data: 2024/3/21 15:58
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "微信支付接口")
@RestController("WxPayController")
@RequestMapping("/api/payment/weixin")
public class WxPayController {
	
	@Resource
	private WeixinService weixinPayService;
//	@Resource
//	private PaymentInfoService paymentInfoServiceService;
	
	@ApiOperation(value = "小程序支付")
	@GetMapping("/createJsapi/{orderNo}")
	public Result<Map<String, String>> createJsapi(
			@ApiParam(name = "orderNo", value = "订单No", required = true)
			@PathVariable("orderNo") String orderNo) {
		log.info("小程序支付：orderNo:{}", orderNo);
		return Result.ok(weixinPayService.createJsapi(orderNo));
	}
	
	@ApiOperation(value = "查询支付状态")
	@GetMapping("/queryPayStatus/{orderNo}")
	public Result<String> queryPayStatus(
			@ApiParam(name = "orderNo", value = "订单No", required = true)
			@PathVariable("orderNo") String orderNo) {
		log.info("查询支付状态：orderNo:{}", orderNo);
		return Result.ok(weixinPayService.queryPayStatus(orderNo, PaymentType.WEIXIN.name()));
	}
}