package com.ssyx.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class ConstantPropertiesUtils implements InitializingBean {
	
	public static String APPID;
	public static String PARTNER;
	public static String PARTNERKEY;
	public static String NOTIFYURL;
	public static String CERT;
	@Value("${ssyx.weixin.appid}")
	private String appid;
	@Value("${ssyx.weixin.partner}")
	private String partner;
	@Value("${ssyx.weixin.partnerkey}")
	private String partnerkey;
	@Value("${ssyx.weixin.notifyurl}")
	private String notifyurl;
	@Value("${ssyx.weixin.cert}")
	private String cert;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		APPID = appid;
		PARTNER = partner;
		PARTNERKEY = partnerkey;
		NOTIFYURL = notifyurl;
		CERT = cert;
	}
}
