package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.order.PaymentInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: PaymentMapper
 * @description: 接口
 * @data: 2024/3/21 16:15
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface PaymentInfoMapper extends BaseMapper<PaymentInfo> {
}
