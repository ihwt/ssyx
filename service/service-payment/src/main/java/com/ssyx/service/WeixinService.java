package com.ssyx.service;

import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: WeixinService
 * @description: 接口
 * @data: 2024/3/21 15:59
 * @author: ihu
 * @version: 1.0
 **/

public interface WeixinService {
	
	/**
	 * 根据订单号下单，生成支付链接
	 */
	Map<String, String> createJsapi(String orderNo);
	
	/**
	 * 根据订单号去微信第三方查询支付状态
	 */
	String queryPayStatus(String orderNo, String paymentType);
	
}
