package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.order.PaymentInfo;

import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: PaymentService
 * @description: 接口
 * @data: 2024/3/21 16:00
 * @author: ihu
 * @version: 1.0
 **/

public interface PaymentInfoService extends IService<PaymentInfo> {
	PaymentInfo getPaymentInfo(String orderNo, Integer paymentType);
	
	PaymentInfo savePaymentInfo(String orderNo, Integer paymentType);
	
	//支付成功
	void paySuccess(String orderNo, Integer paymentType, Map<String, String> paramMap);
}
