package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.constant.RabbitMQConstant;
import com.ssyx.enums.PaymentStatus;
import com.ssyx.exception.SsyxException;
import com.ssyx.mapper.PaymentInfoMapper;
import com.ssyx.model.order.OrderInfo;
import com.ssyx.model.order.PaymentInfo;
import com.ssyx.order.OrderFeignClient;
import com.ssyx.result.ResultCodeEnum;
import com.ssyx.service.PaymentInfoService;
import com.ssyx.service.RabbitMQService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: PaymentServiceImpl
 * @description: 类
 * @data: 2024/3/21 16:14
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class PaymentInfoServiceImpl extends ServiceImpl<PaymentInfoMapper, PaymentInfo> implements PaymentInfoService {
	@Resource
	private OrderFeignClient orderFeignClient;
	@Resource
	private RabbitMQService rabbitMQService;
	
	@Override
	public PaymentInfo getPaymentInfo(String orderNo, Integer paymentType) {
		return getOne(Wrappers.<PaymentInfo>lambdaQuery().eq(PaymentInfo::getOrderNo, orderNo).eq(PaymentInfo::getPaymentType, paymentType));
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public PaymentInfo savePaymentInfo(String orderNo, Integer paymentType) {
		OrderInfo order = orderFeignClient.getOrderInfo(orderNo);
		if (order == null) {
			log.error("根据订单编号获取订单信息为空:{}", orderNo);
			throw new SsyxException(ResultCodeEnum.DATA_ERROR);
		}
		// 保存交易记录
		PaymentInfo paymentInfo = new PaymentInfo();
		paymentInfo.setCreateTime(new Date());
		paymentInfo.setOrderId(order.getId());
		paymentInfo.setPaymentType(paymentType);
		paymentInfo.setUserId(order.getUserId());
		paymentInfo.setOrderNo(order.getOrderNo());
		paymentInfo.setPaymentStatus(PaymentStatus.UNPAID.getCode());
		paymentInfo.setSubject(order.getNickName());
		paymentInfo.setTotalAmount(order.getTotalAmount());
//		paymentInfo.setTotalAmount(new BigDecimal("0.01"));
		
		save(paymentInfo);
		return paymentInfo;
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void paySuccess(String orderNo, Integer paymentType, Map<String, String> paramMap) {
		PaymentInfo paymentInfo = getPaymentInfo(orderNo, paymentType);
		if (!paymentInfo.getPaymentStatus().equals(PaymentStatus.UNPAID.getCode())) {
			return;
		}
		PaymentInfo paymentInfoUpd = new PaymentInfo();
		paymentInfoUpd.setPaymentStatus(PaymentStatus.PAID.getCode());
//		String tradeNo = paymentType.equals(PaymentType.WEIXIN.getCode()) ? paramMap.get("ransaction_id") : paramMap.get(
//				"trade_no");
		paymentInfoUpd.setTradeNo(String.valueOf(System.currentTimeMillis()));
		paymentInfoUpd.setCallbackTime(new Date());
//		paymentInfoUpd.setCallbackContent(paramMap.toString());
		paymentInfoUpd.setCallbackContent(new Date().toString());
		update(paymentInfoUpd, new LambdaQueryWrapper<PaymentInfo>().eq(PaymentInfo::getOrderNo, orderNo));
		// 表示交易成功！
		//发送消息
		rabbitMQService.sendMessage(RabbitMQConstant.EXCHANGE_PAY_DIRECT, RabbitMQConstant.ROUTING_PAY_SUCCESS, orderNo);
	}
}
