package com.ssyx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.model.acl.Role;
import com.ssyx.result.Result;
import com.ssyx.service.RoleService;
import com.ssyx.vo.acl.RoleQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: RoleController
 * @description: 用户角色管理
 * @data: 2024/3/7 13:52
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "用户角色管理")
@RestController("RoleController")
@RequestMapping("/admin/acl/role")
public class RoleController {
	
	@Resource
	private RoleService roleService;
	
	@ApiOperation(value = "获取角色分页列表")
	@GetMapping("{page}/{limit}")
	public Result<IPage<Role>> index(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			
			@ApiParam(name = "roleQueryVo", value = "查询对象", required = false)
			RoleQueryVo roleQueryVo) {
		log.info("获取角色分页列表：page:{},limit:{},roleQueryVo{}", page, limit, roleQueryVo);
		Page<Role> pageParam = new Page<>(page, limit);
		return Result.ok(roleService.selectPage(pageParam, roleQueryVo));
	}
	
	@ApiOperation(value = "获取角色")
	@GetMapping("get/{id}")
	public Result<Role> get(@PathVariable Long id) {
		log.info("获取角色：id:{}", id);
		return Result.ok(roleService.getById(id));
	}
	
	@ApiOperation(value = "新增角色")
	@PostMapping("save")
	public Result<?> save(@RequestBody Role role) {
		log.info("新增角色：{}", role);
		return isSuccess(roleService.saveRole(role));
	}
	
	@ApiOperation(value = "修改角色")
	@PutMapping("update")
	public Result<?> updateById(@RequestBody Role role) {
		log.info("修改角色：{}", role);
		return isSuccess(roleService.updateById(role));
	}
	
	@ApiOperation(value = "删除角色")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("删除角色：id:{}", id);
		return isSuccess(roleService.removeById(id));
	}
	
	@ApiOperation(value = "根据id列表删除角色")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("根据id列表删除角色：idList:{}", idList);
		return isSuccess(roleService.removeByIds(idList));
	}
	
	
	private Result<Object> isSuccess(Boolean is_success) {
		if (is_success) {
			return Result.ok(null);
		}
		return Result.fail(null);
	}
}
