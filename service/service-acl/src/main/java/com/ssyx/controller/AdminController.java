package com.ssyx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.model.acl.Admin;
import com.ssyx.result.Result;
import com.ssyx.service.AdminService;
import com.ssyx.service.RoleService;
import com.ssyx.vo.acl.AdminQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: AdminController
 * @description: 用户管理
 * @data: 2024/3/7 14:26
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "用户管理")
@RestController("AdminController")
@RequestMapping("/admin/acl/user")
public class AdminController {
	
	@Resource
	private AdminService adminService;
	@Resource
	private RoleService roleService;
	
	@ApiOperation(value = "根据用户获取角色数据")
	@GetMapping("/toAssign/{adminId}")
	public Result<Map<String, Object>> toAssign(@PathVariable Long adminId) {
		log.info("根据用户获取角色数据：adminId:{}", adminId);
		return Result.ok(roleService.findRoleByUserId(adminId));
	}
	
	@ApiOperation(value = "根据用户分配角色")
	@PostMapping("/doAssign")
	public Result<?> doAssign(@RequestParam Long adminId, @RequestParam Long[] roleId) {
		log.info("根据用户分配角色：adminId:{}，roleIds:{}", adminId, roleId);
		roleService.saveUserRoleRealtionShip(adminId, roleId);
		return Result.ok(null);
	}
	
	@ApiOperation("用户列表")
	@GetMapping("{current}/{limit}")
	public Result<IPage<Admin>> list(@PathVariable Long current,
	                                 @PathVariable Long limit,
	                                 AdminQueryVo adminQueryVo) {
		log.info("用户列表：current:{}，limit:{}，adminQueryVo:{}", current, limit, adminQueryVo);
		Page<Admin> pageParam = new Page<>(current, limit);
		return Result.ok(adminService.selectPageUser(pageParam, adminQueryVo));
	}
	
	@ApiOperation("根据id查询")
	@GetMapping("get/{id}")
	public Result<Admin> get(@PathVariable Long id) {
		log.info("根据id查询：id:{}", id);
		return Result.ok(adminService.getById(id));
	}
	
	@ApiOperation("添加用户")
	@PostMapping("save")
	public Result<?> save(@RequestBody Admin admin) {
		log.info("添加用户：admin:{}", admin);
		adminService.saveAdmin(admin);
		return Result.ok(null);
	}
	
	@ApiOperation("修改用户")
	@PutMapping("update")
	public Result<?> update(@RequestBody Admin admin) {
		log.info("修改用户：admin:{}", admin);
		adminService.updateAdmin(admin);
		return Result.ok(null);
	}
	
	@ApiOperation("根据id删除用户")
	@DeleteMapping("remove/{id}")
	public Result<?> remove(@PathVariable Long id) {
		log.info("根据id删除用户：id:{}", id);
		adminService.removeById(id);
		return Result.ok(null);
	}
	
	@ApiOperation("批量删除")
	@DeleteMapping("batchRemove")
	public Result<?> batchRemove(@RequestBody List<Long> idList) {
		log.info("批量删除：idList:{}", idList);
		adminService.removeByIds(idList);
		return Result.ok(null);
	}
	
}
