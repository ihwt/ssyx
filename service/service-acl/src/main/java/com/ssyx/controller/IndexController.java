package com.ssyx.controller;

import com.ssyx.auth.AuthThreadLocal;
import com.ssyx.constant.RedisConstant;
import com.ssyx.model.acl.Admin;
import com.ssyx.result.Result;
import com.ssyx.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: IndexController
 * @description: 登录
 * @data: 2024/3/7 13:28
 * @author: ihu
 * @version: 1.0
 **/
@Slf4j
@Api(tags = "登录管理")
@RestController("IndexController")
@RequestMapping("/admin/acl/index")
public class IndexController {
	
	@Resource
	private AdminService adminService;
	@Resource
	private RedisTemplate<String, Object> redisTemplate;
	
	@ApiOperation("登录")
	@PostMapping("login")
	public Result<Map<String, String>> login(@RequestBody Admin admin) {
		log.info("账户登录：admin:{}", admin);
		return Result.ok(adminService.login(admin));
	}
	
	@ApiOperation("获取信息")
	@GetMapping("info")
	public Result<Map<String, Object>> info() {
		log.info("获取信息：id:{}", AuthThreadLocal.getId());
		return Result.ok(adminService.info());
	}
	
	@ApiOperation("退出")
	@PostMapping("logout")
	public Result<?> logout() {
		log.info("退出");
		redisTemplate.delete(RedisConstant.ADMIN_KEY_PREFIX + AuthThreadLocal.getId());
		redisTemplate.delete(RedisConstant.ADMIN_LOGIN_KEY_PREFIX + AuthThreadLocal.getId());
		return Result.ok(null);
	}
	
}
