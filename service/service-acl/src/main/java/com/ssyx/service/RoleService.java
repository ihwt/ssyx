package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.acl.Role;
import com.ssyx.vo.acl.RoleQueryVo;

import java.util.Map;

public interface RoleService extends IService<Role> {
	IPage<Role> selectPage(Page<Role> pageParam, RoleQueryVo roleQueryVo);
	
	Map<String, Object> findRoleByUserId(Long adminId);
	
	void saveUserRoleRealtionShip(Long adminId, Long[] roleIds);
	
	Boolean saveRole(Role role);
}
