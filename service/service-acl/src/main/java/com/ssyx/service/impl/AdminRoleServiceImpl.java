package com.ssyx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.mapper.AdminRoleMapper;
import com.ssyx.model.acl.AdminRole;
import com.ssyx.service.AdminRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @program: ssyx-parent
 * @className: AdminRoleServiceImpl
 * @description: 分配角色
 * @data: 2024/3/7 14:23
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class AdminRoleServiceImpl extends ServiceImpl<AdminRoleMapper, AdminRole>
		implements AdminRoleService {
	
}
