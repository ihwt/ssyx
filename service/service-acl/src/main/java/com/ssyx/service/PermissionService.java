package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.acl.Permission;

import java.util.List;

/**
 * @program: ssyx-parent
 * @className: PermissionService
 * @description: 权限服务
 * @data: 2024/3/7 15:05
 * @author: ihu
 * @version: 1.0
 **/

public interface PermissionService extends IService<Permission> {
	
	//获取所有菜单列表
	List<Permission> queryAllMenu();
	
	//递归删除
	void removeChildById(Long id);
	
	List<Permission> toAssign(Long roleId);
	
	void doAssign(Long roleId, Long[] permissionId);
}
