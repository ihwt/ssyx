package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.acl.AdminRole;

public interface AdminRoleService extends IService<AdminRole> {
}
