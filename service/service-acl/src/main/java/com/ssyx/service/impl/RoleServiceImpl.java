package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.exception.SsyxException;
import com.ssyx.mapper.AdminRoleMapper;
import com.ssyx.mapper.RoleMapper;
import com.ssyx.model.acl.AdminRole;
import com.ssyx.model.acl.Role;
import com.ssyx.result.ResultCodeEnum;
import com.ssyx.service.RoleService;
import com.ssyx.vo.acl.RoleQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: ssyx-parent
 * @className: RoleServiceImpl
 * @description: 角色管理
 * @data: 2024/3/7 13:54
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
	
	@Resource
	private AdminRoleMapper adminRoleMapper;
	
	/**
	 * 角色分页列表
	 */
	@Override
	public IPage<Role> selectPage(Page<Role> pageParam, RoleQueryVo roleQueryVo) {
		// 角色名称
		String roleName = roleQueryVo.getRoleName();
		// 调用mapper方法实现条件分页查询
		return baseMapper.selectPage(pageParam, Wrappers.<Role>lambdaQuery().like(!StringUtils.isEmpty(roleName),
				Role::getRoleName, roleName));
	}
	
	/**
	 * 根据用户获取角色数据
	 */
	@Override
	public Map<String, Object> findRoleByUserId(Long adminId) {
		// 查询所有的角色
		List<Role> allRolesList = list();
		// 拥有的角色id
		List<AdminRole> existUserRoleList =
				adminRoleMapper.selectList(new QueryWrapper<AdminRole>().eq("admin_id", adminId).select(
						"role_id"));
		List<Long> existRoleList = existUserRoleList.stream().map(AdminRole::getRoleId).collect(Collectors.toList());
		// 对角色进行分类
		List<Role> assignRoles =
				allRolesList.stream().filter(role -> existRoleList.contains(role.getId())).collect(Collectors.toList());
		// 创建Map对象封装数据
		Map<String, Object> roleMap = new HashMap<>();
		roleMap.put("assignRoles", assignRoles);
		roleMap.put("allRolesList", allRolesList);
		return roleMap;
	}
	
	/**
	 * 分配角色
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void saveUserRoleRealtionShip(Long adminId, Long[] roleIds) {
		// 删除用户分配的角色数据
		adminRoleMapper.delete(new QueryWrapper<AdminRole>().eq("admin_id", adminId));
		// 分配新的角色
		// todo:可能影响性能
		for (Long roleId : roleIds) {
			if (StringUtils.isEmpty(roleId)) continue;
			AdminRole userRole = new AdminRole();
			userRole.setAdminId(adminId);
			userRole.setRoleId(roleId);
			adminRoleMapper.insert(userRole);
		}
	}
	
	@Override
	public Boolean saveRole(Role role) {
		if (role.getRoleName() == null || role.getRoleName().isEmpty() || role.getRoleName().equals(" ")) {
			log.error("新增角色失败，角色名称不能为空");
			throw new SsyxException(ResultCodeEnum.ILLEGAL_PARAM);
		}
		return save(role);
	}
	
	
}
