package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.acl.Admin;
import com.ssyx.vo.acl.AdminQueryVo;

import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: AdminService
 * @description: 用户管理
 * @data: 2024/3/7 15:24
 * @author: ihu
 * @version: 1.0
 **/

public interface AdminService extends IService<Admin> {
	IPage<Admin> selectPageUser(Page<Admin> pageParam, AdminQueryVo adminQueryVo);
	
	void saveAdmin(Admin admin);
	
	Map<String, String> login(Admin admin);
	
	Map<String, Object> info();
	
	void updateAdmin(Admin admin);
}
