package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.acl.RolePermission;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: PermissionRoleMapper
 * @description:
 * @data: 2024/3/7 18:06
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface RolePermissionMapper extends BaseMapper<RolePermission> {
}
