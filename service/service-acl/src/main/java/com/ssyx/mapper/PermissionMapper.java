package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.acl.Permission;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: PermissionMapper
 * @description: 权限服务
 * @data: 2024/3/7 15:04
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {
}
