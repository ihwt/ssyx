package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.acl.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: AdminMapper
 * @description: 用户管理
 * @data: 2024/3/7 15:29
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface AdminMapper extends BaseMapper<Admin> {
}
