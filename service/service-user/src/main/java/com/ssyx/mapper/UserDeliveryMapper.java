package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.enums.user.UserDelivery;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: UserDeliveryMapper
 * @description: 接口
 * @data: 2024/3/18 13:56
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface UserDeliveryMapper extends BaseMapper<UserDelivery> {
}
