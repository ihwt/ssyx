package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.enums.user.Leader;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: LeaderMapper
 * @description: 接口
 * @data: 2024/3/18 13:57
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface LeaderMapper extends BaseMapper<Leader> {
}
