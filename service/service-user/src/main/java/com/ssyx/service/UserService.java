package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.enums.user.User;
import com.ssyx.vo.user.LeaderAddressVo;
import com.ssyx.vo.user.UserLoginVo;

import java.util.Map;

/**
 * @program: ssyx-parent
 * @className: UserService
 * @description: 接口
 * @data: 2024/3/18 13:55
 * @author: ihu
 * @version: 1.0
 **/

public interface UserService extends IService<User> {
	
	LeaderAddressVo getLeaderAddressVoByUserId(Long userId);
	
	// 获取当前登录用户信息
	UserLoginVo getUserLoginVo(Long userId);
	
	Map<String, Object> wxLogin(String code);
	
}
