package com.ssyx.api;

import com.ssyx.service.UserService;
import com.ssyx.vo.user.LeaderAddressVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/api/user/leader")
public class LeaderAddressApiController {
	
	@Resource
	private UserService userService;
	
	//根据userId查询提货点和团长信息
	@GetMapping("/inner/getUserAddressByUserId/{userId}")
	public LeaderAddressVo getUserAddressByUserId(@PathVariable("userId") Long userId) {
		log.info("FeignClient(根据分类id获取分类信息)：{}", userId);
		return userService.getLeaderAddressVoByUserId(userId);
	}
}
