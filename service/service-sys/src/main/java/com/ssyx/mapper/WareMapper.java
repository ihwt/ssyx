package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.sys.Ware;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: WareMapper
 * @description:
 * @data: 2024/3/9 16:53
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface WareMapper extends BaseMapper<Ware> {
}
