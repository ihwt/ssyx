package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.sys.RegionWare;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: RegionWareMapper
 * @description: 开通区域列表
 * @data: 2024/3/9 16:38
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface RegionWareMapper extends BaseMapper<RegionWare> {
}
