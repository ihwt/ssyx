package com.ssyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ssyx.model.sys.Region;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: ssyx-parent
 * @className: RegionMapper
 * @description:
 * @data: 2024/3/9 16:48
 * @author: ihu
 * @version: 1.0
 **/

@Mapper
public interface RegionMapper extends BaseMapper<Region> {
}
