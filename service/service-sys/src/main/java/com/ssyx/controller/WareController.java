package com.ssyx.controller;

/**
 * @program: ssyx-parent
 * @className: WareController
 * @description: 获取全部仓库
 * @data: 2024/3/9 16:51
 * @author: ihu
 * @version: 1.0
 **/

import com.ssyx.model.sys.RegionWare;
import com.ssyx.model.sys.Ware;
import com.ssyx.result.Result;
import com.ssyx.service.WareService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: RegionWareController
 * @description: 仓库详情
 * @data: 2024/3/9 16:30
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "仓库详情")
@RestController("WareController")
@RequestMapping("/admin/sys/ware")
public class WareController {
	
	@Resource
	private WareService wareService;
	
	@ApiOperation(value = "获取全部仓库")
	@GetMapping("findAllList")
	public Result<List<Ware>> findAllList() {
		log.info("获取全部仓库");
		return Result.ok(wareService.list());
	}
	
	@ApiOperation(value = "新增仓库")
	@PostMapping("save")
	public Result<?> save(@RequestBody RegionWare regionWare) {
		log.info("新增仓库：regionWare:{}", regionWare);
		wareService.saveWare(regionWare);
		return Result.ok(null);
	}
}
