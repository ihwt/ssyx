package com.ssyx.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ssyx.model.sys.Region;
import com.ssyx.result.Result;
import com.ssyx.service.RegionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: ssyx-parent
 * @className: RegionController
 * @description: 区域详情
 * @data: 2024/3/9 16:45
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "区域详情")
@RestController("RegionController")
@RequestMapping("/admin/sys/region")
public class RegionController {
	
	@Resource
	private RegionService regionService;
	
	@ApiOperation(value = "根据关键字获取地区列表")
	@GetMapping("findRegionByKeyword/{keyword}")
	public Result<List<Region>> findSkuInfoByKeyword(@PathVariable("keyword") String keyword) {
		log.info("根据关键字获取地区列表：keyword:{}", keyword);
		return Result.ok(regionService.list(Wrappers.<Region>lambdaQuery().like(Region::getName, keyword)));
	}
}