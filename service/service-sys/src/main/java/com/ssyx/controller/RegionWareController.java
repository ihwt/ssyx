package com.ssyx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ssyx.model.sys.RegionWare;
import com.ssyx.result.Result;
import com.ssyx.service.RegionWareService;
import com.ssyx.vo.sys.RegionWareQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @program: ssyx-parent
 * @className: RegionWareController
 * @description: 开通区域列表
 * @data: 2024/3/9 16:30
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Api(tags = "开通区域管理")
@RestController("RegionWareController")
@RequestMapping("/admin/sys/regionWare")
@SuppressWarnings({"unchecked", "rawtypes"})
public class RegionWareController {
	
	@Resource
	private RegionWareService regionWareService;
	
	//开通区域列表
	@ApiOperation(value = "获取开通区域列表")
	@GetMapping("{page}/{limit}")
	public Result<IPage<RegionWare>> index(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			@ApiParam(name = "regionWareVo", value = "查询对象", required = false)
			RegionWareQueryVo regionWareQueryVo) {
		log.info("获取开通区域列表：page:{},limit:{},roleQueryVo{}", page, limit, regionWareQueryVo);
		Page<RegionWare> pageParam = new Page<>(page, limit);
		return Result.ok(regionWareService.selectPage(pageParam, regionWareQueryVo));
	}
	
	/**
	 * 添加开通区域
	 **/
	@ApiOperation(value = "新增开通区域")
	@PostMapping("save")
	public Result save(@RequestBody RegionWare regionWare) {
		log.info("添加开通区域：regionWare:{}", regionWare);
		regionWareService.saveRegionWare(regionWare);
		return Result.ok(null);
	}
	
	/**
	 * 删除开通区域
	 **/
	@ApiOperation(value = "删除开通区域")
	@DeleteMapping("remove/{id}")
	public Result remove(@PathVariable Long id) {
		log.info("删除开通区域：id:{}", id);
		regionWareService.removeById(id);
		return Result.ok(null);
	}
	
	/**
	 * 修改开通区域
	 **/
	@ApiOperation(value = "取消开通区域")
	@PostMapping("updateStatus/{id}/{status}")
	public Result updateStatus(@PathVariable Long id, @PathVariable Integer status) {
		log.info("取消开通区域：id:{},status:{}", id, status);
		regionWareService.updateStatus(id, status);
		return Result.ok(null);
	}
}
