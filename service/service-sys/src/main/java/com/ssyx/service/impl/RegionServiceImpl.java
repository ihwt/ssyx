package com.ssyx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.mapper.RegionMapper;
import com.ssyx.model.sys.Region;
import com.ssyx.service.RegionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @program: ssyx-parent
 * @className: RegionServiceImpl
 * @description:
 * @data: 2024/3/9 16:47
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements RegionService {

}
