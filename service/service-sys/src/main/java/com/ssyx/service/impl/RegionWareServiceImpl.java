package com.ssyx.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ssyx.exception.SsyxException;
import com.ssyx.mapper.RegionWareMapper;
import com.ssyx.model.sys.RegionWare;
import com.ssyx.result.ResultCodeEnum;
import com.ssyx.service.RegionWareService;
import com.ssyx.vo.sys.RegionWareQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * @program: ssyx-parent
 * @className: RegionWareServiceImpl
 * @description: 开通区域列表
 * @data: 2024/3/9 16:38
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
@SuppressWarnings({"unchecked", "rawtypes"})
public class RegionWareServiceImpl extends ServiceImpl<RegionWareMapper, RegionWare> implements RegionWareService {
	
	@Resource
	private RegionWareMapper regionWareMapper;
	
	/**
	 * 开通区域列表
	 */
	@Override
	public IPage<RegionWare> selectPage(Page<RegionWare> pageParam, RegionWareQueryVo regionWareQueryVo) {
		String keyword = regionWareQueryVo.getKeyword();
		return baseMapper.selectPage(pageParam, Wrappers.<RegionWare>lambdaQuery().like(!StringUtils.isEmpty(keyword),
						RegionWare::getRegionName, keyword)
				.or().like(!StringUtils.isEmpty(keyword), RegionWare::getWareName, keyword));
	}
	
	/**
	 * 新增开通区域
	 */
	@Override
	public void saveRegionWare(RegionWare regionWare) {
		if (regionWare.getWareId() == null || regionWare.getRegionName() == null) {
			log.error("开通失败，参数错误");
			throw new SsyxException(ResultCodeEnum.ILLEGAL_PARAM);
		}
		//判断是否已经开通
		Integer count = regionWareMapper.selectCount(Wrappers.<RegionWare>lambdaQuery().eq(RegionWare::getRegionName,
				regionWare.getRegionName()).or().eq(RegionWare::getRegionName,
				regionWare.getRegionName() + "市"));
		if (count > 0) {
			log.error("开通失败，该区域已开通");
			throw new SsyxException(ResultCodeEnum.REGION_OPEN);
		}
		save(regionWare);
	}
	
	@Override
	public void updateStatus(Long id, Integer status) {
		RegionWare regionWare = getById(id);
		regionWare.setStatus(status);
		updateById(regionWare);
	}
	
}
