package com.ssyx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.sys.RegionWare;
import com.ssyx.vo.sys.RegionWareQueryVo;

/**
 * @program: ssyx-parent
 * @className: RegionWareService
 * @description: 开通区域列表
 * @data: 2024/3/9 16:37
 * @author: ihu
 * @version: 1.0
 **/

public interface RegionWareService extends IService<RegionWare> {
	
	IPage<RegionWare> selectPage(Page<RegionWare> pageParam,
	                             RegionWareQueryVo regionWareQueryVo);
	
	void saveRegionWare(RegionWare regionWare);
	
	void updateStatus(Long id, Integer status);
}
