package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.sys.RegionWare;
import com.ssyx.model.sys.Ware;

/**
 * @program: ssyx-parent
 * @className: WareService
 * @description:
 * @data: 2024/3/9 16:52
 * @author: ihu
 * @version: 1.0
 **/

public interface WareService extends IService<Ware> {
	void saveWare(RegionWare regionWare);
}
