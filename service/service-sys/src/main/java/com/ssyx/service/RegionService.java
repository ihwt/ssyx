package com.ssyx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ssyx.model.sys.Region;

/**
 * @program: ssyx-parent
 * @className: RegionService
 * @description:
 * @data: 2024/3/9 16:46
 * @author: ihu
 * @version: 1.0
 **/

public interface RegionService extends IService<Region> {
}
