package com.ssyx.enums.user;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ssyx.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @program: ssyx-parent
 * @className: LeaderBank
 * @description: LeaderBank
 * @data: 2024/3/6 19:25
 * @author: ihu
 * @version: 1.0
 **/

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "LeaderBank")
@TableName("leader_bank")
public class LeaderBank extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "团长ID")
	@TableField("leader_id")
	private String leaderId;
	
	@ApiModelProperty(value = "账户类型(微信,银行)")
	@TableField("account_type")
	private String accountType;
	
	@ApiModelProperty(value = "银行名称")
	@TableField("bank_name")
	private String bankName;
	
	@ApiModelProperty(value = "银行账号")
	@TableField("bank_account_no")
	private String bankAccountNo;
	
	@ApiModelProperty(value = "银行账户名")
	@TableField("bank_account_name")
	private String bankAccountName;
	
	@ApiModelProperty(value = "微信ID")
	@TableField("wechat_id")
	private String wechatId;
	
}