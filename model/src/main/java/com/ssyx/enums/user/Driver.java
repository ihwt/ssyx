package com.ssyx.enums.user;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ssyx.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @program: ssyx-parent
 * @className: Driver
 * @description: Driver
 * @data: 2024/3/6 19:25
 * @author: ihu
 * @version: 1.0
 **/

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "Driver")
@TableName("driver")
public class Driver extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "名称")
	@TableField("name")
	private String name;
	
	@ApiModelProperty(value = "手机")
	@TableField("phone")
	private String phone;
	
	@ApiModelProperty(value = "仓库id")
	@TableField("ware_id")
	private Long wareId;
	
}