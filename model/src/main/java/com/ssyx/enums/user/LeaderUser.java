package com.ssyx.enums.user;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ssyx.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @program: ssyx-parent
 * @className: LeaderUser
 * @description: LeaderUser
 * @data: 2024/3/6 19:25
 * @author: ihu
 * @version: 1.0
 **/

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "LeaderUser")
@TableName("leader_user")
public class LeaderUser extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "团长ID")
	@TableField("leader_id")
	private String leaderId;
	
	@ApiModelProperty(value = "userId")
	@TableField("user_id")
	private Long userId;
	
}