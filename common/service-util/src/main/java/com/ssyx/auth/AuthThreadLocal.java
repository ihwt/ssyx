package com.ssyx.auth;

import com.ssyx.vo.user.UserLoginVo;

//ThreadLocal工具类
public class AuthThreadLocal {
	
	//用户id
	private static final ThreadLocal<Long> id = new ThreadLocal<>();
	private static final ThreadLocal<Long> ware = new ThreadLocal<>();
	
	// 用户信息对象
	private static final ThreadLocal<UserLoginVo> userLoginVo = new ThreadLocal<>();
	
	public static Long getId() {
		return id.get();
	}
	
	//userId操作的方法
	public static void setId(Long _id) {id.set(_id);}
	
	public static void removeId() {id.remove();}
	
	public static Long getWare() {return ware.get();}
	
	//userId操作的方法
	public static void setWare(Long _ware) {ware.set(_ware);}
	
	public static void removeWare() {ware.remove();}
	
	public static UserLoginVo getUserLoginVo() {return userLoginVo.get();}
	
	public static void setUserLoginVo(UserLoginVo _userLoginVo) {
		userLoginVo.set(_userLoginVo);
	}
	
	public static void removeUserLoginVo() {userLoginVo.remove();}
	
	
}
