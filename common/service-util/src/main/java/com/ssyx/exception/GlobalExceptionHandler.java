package com.ssyx.exception;

import com.ssyx.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: ssyx-parent
 * @className: GlobalExceptionHandler
 * @description: 全局异常处理
 * @data: 2024/3/6 16:57
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {
	
	/**
	 * 处理全局异常
	 */
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Result<?> exceptionHandler(Exception e) {
		log.error("Exception全局异常信息:{}", e.getMessage(), e);
		return Result.fail(e.getMessage());
	}
	
	/**
	 * 自定义异常处理方法
	 */
	@ExceptionHandler(SsyxException.class)
	@ResponseBody
	public Result<?> exceptionHandler(SsyxException e) {
		log.error("SsyxException自定义异常信息:{}", e.getMessage(), e);
		return Result.build(null, e.getCode(), e.getMessage());
	}
}
