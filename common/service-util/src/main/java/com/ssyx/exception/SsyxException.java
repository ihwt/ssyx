package com.ssyx.exception;

import com.ssyx.result.ResultCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @program: ssyx-parent
 * @className: SsyxException
 * @description: 自定义异常处理
 * @data: 2024/3/6 16:59
 * @author: ihu
 * @version: 1.0
 **/

@Data
@EqualsAndHashCode(callSuper = true)
public class SsyxException extends RuntimeException {
	
	//异常状态码
	private Integer code;
	
	/**
	 * 通过状态码和错误消息创建异常对象
	 */
	public SsyxException(String message, Integer code) {
		super(message);
		this.code = code;
	}
	
	/**
	 * 接收枚举类型对象
	 */
	public SsyxException(ResultCodeEnum resultCodeEnum) {
		super(resultCodeEnum.getMessage());
		this.code = resultCodeEnum.getCode();
	}
	
	@Override
	public String toString() {
		return "SsyxException{" +
				"code=" + code +
				", message=" + this.getMessage() +
				'}';
	}
}
