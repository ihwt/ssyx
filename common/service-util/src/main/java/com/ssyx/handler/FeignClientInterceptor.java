package com.ssyx.handler;

import com.ssyx.constant.GlobalConstant;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: ssyx-parent
 * @className: OpenFeignConfiguration
 * @description: 类
 * @data: 2024/3/18 19:57
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
public class FeignClientInterceptor implements RequestInterceptor {
	
	@Override
	public void apply(RequestTemplate requestTemplate) {
		requestTemplate.header("token", GlobalConstant.VALIDATE);
		log.info("FeignClient(onlyToken)：{}", GlobalConstant.VALIDATE);
	}
}
