package com.ssyx.config;

import com.ssyx.handler.FeignClientInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: ssyx-parent
 * @className: FeignConfiguration
 * @description: 类
 * @data: 2024/3/19 11:18
 * @author: ihu
 * @version: 1.0
 **/

@Configuration
public class FeignConfiguration {
	
	@Bean
	public FeignClientInterceptor feignClientInterceptor() {
		return new FeignClientInterceptor();
	}
}
