package com.ssyx.config;

import com.ssyx.handler.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.annotation.Resource;

@Configuration
public class LoginMvcConfiguration extends WebMvcConfigurationSupport {
	
	@Resource
	private RedisTemplate<String, Object> redisTemplate;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoginInterceptor(redisTemplate))
				.addPathPatterns("/**")
				.excludePathPatterns("/api/user/weixin/wxLogin/*")
				.excludePathPatterns("/admin/acl/index/login");
		super.addInterceptors(registry);
	}
}
