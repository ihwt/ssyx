package com.ssyx.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @program: ssyx-parent
 * @className: RabbitService
 * @description: rabbitmq类
 * @data: 2024/3/15 14:46
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Service
public class RabbitMQService {
	
	//  引入操作rabbitmq 的模板
	@Resource
	private RabbitTemplate rabbitTemplate;
	
	/**
	 * 发送消息
	 *
	 * @param exchange   交换机
	 * @param routingKey 路由键
	 * @param message    消息
	 */
	public boolean sendMessage(String exchange, String routingKey, Object message) {
		//  调用发送数据的方法
		rabbitTemplate.convertAndSend(exchange, routingKey, message);
		return true;
	}
	
	/**
	 * 发送延迟消息的方法
	 *
	 * @param exchange   交换机
	 * @param routingKey 路由键
	 * @param message    消息内容
	 * @param delayTime  延迟时间
	 */
	public boolean sendDelayMessage(String exchange, String routingKey, Object message, int delayTime) {
		
		//  在发送消息的时候设置延迟时间
		rabbitTemplate.convertAndSend(exchange, routingKey, message, msg -> {
			//  设置一个延迟时间
			msg.getMessageProperties().setDelay(delayTime * 1000);
			return msg;
		});
		return true;
	}
}