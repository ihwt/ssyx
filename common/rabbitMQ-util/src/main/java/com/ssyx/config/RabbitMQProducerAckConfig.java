package com.ssyx.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @program: ssyx-parent
 * @className: RabbitMQProducerAckConfig
 * @description: 类
 * @data: 2024/3/15 14:48
 * @author: ihu
 * @version: 1.0
 **/

@Slf4j
@Component
public class RabbitMQProducerAckConfig implements RabbitTemplate.ReturnCallback, RabbitTemplate.ConfirmCallback {
	
	//  我们发送消息使用的是 private RabbitTemplate rabbitTemplate; 对象
	//  如果不做设置的话 当前的rabbitTemplate 与当前的配置类没有任何关系！
	@Resource
	private RabbitTemplate rabbitTemplate;
	
	//  设置 表示修饰一个非静态的void方法，在服务器加载Servlet的时候运行。并且只执行一次！
	@PostConstruct
	public void init() {
		rabbitTemplate.setReturnCallback(this);
		rabbitTemplate.setConfirmCallback(this);
	}
	
	/**
	 * 表示消息是否正确发送到了交换机上
	 *
	 * @param correlationData 消息的载体
	 * @param ack             判断是否发送到交换机上
	 * @param cause           原因
	 */
	@Override
	public void confirm(CorrelationData correlationData, boolean ack, String cause) {
		if (ack) {
			log.info("消息发送成功！");
		} else {
			log.info("消息发送失败:{}！", cause);
		}
	}
	
	/**
	 * 消息如果没有正确发送到队列中，则会走这个方法！如果消息被正常处理，则这个方法不会走！
	 */
	@Override
	public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
		log.error("消息主体：{},应答码：{},描述：{},消息使用的交换器-exchange:{},消息使用的路由键-routing:{}", new String(message.getBody()), replyCode
				, replyText, exchange, routingKey);
	}
}
