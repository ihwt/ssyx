package com.ssyx.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: ssyx-parent
 * @className: RabbitMQConfig
 * @description: mq配置类
 * @data: 2024/3/15 14:47
 * @author: ihu
 * @version: 1.0
 **/

@Configuration
public class RabbitMQConfig {
	@Bean
	public MessageConverter messageConverter() {
		return new Jackson2JsonMessageConverter();
	}
}
