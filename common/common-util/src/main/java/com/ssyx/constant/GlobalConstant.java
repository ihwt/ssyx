package com.ssyx.constant;

/**
 * @program: ssyx-parent
 * @className: GlobalConstant
 * @description: 全局公共常量
 * @data: 2024/3/7 15:10
 * @author: ihu
 * @version: 1.0
 **/

public class GlobalConstant {
	
	// integer
	public static final Integer INTEGER_ZERO = 0;
	public static final Integer INTEGER_ONE = 1;
	public static final Integer INTEGER_TWO = 2;
	public static final Integer INTEGER_THREE = 3;
	// long
	public static final Long LONG_ZERO = 0L;
	public static final Long LONG_ONE = 1L;
	public static final Long LONG_TWO = 2L;
	
	public static final String VALIDATE = "Request does not require verification";
}
