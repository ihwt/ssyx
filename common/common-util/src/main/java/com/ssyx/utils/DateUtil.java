package com.ssyx.utils;

import com.ssyx.constant.GlobalConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期操作工具类
 */
@Slf4j
public class DateUtil {
	
	private static final String dateFormat = "yyyy-MM-dd";
	private static final String timeFormat = "HH:mm:ss";
	
	/**
	 * 格式化日期
	 */
	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(date);
		
	}
	
	/**
	 * 格式化日期
	 */
	public static String formatTime(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);
		return sdf.format(date);
		
	}
	
	public static Date parseTime(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			log.error("日期转换错误", e);
		}
		return null;
	}
	
	/**
	 * 截取比较断两个日期对象的field处的值 。
	 * 如果第一个日期小于、等于、大于第二个，则对应返回负整数、0、正整数
	 *
	 * @param date1 第一个日期对象，非null
	 * @param date2 第二个日期对象，非null
	 * @param field Calendar中的阈值
	 */
	public static int truncatedCompareTo(final Date date1, final Date date2, final int field) {
		return DateUtils.truncatedCompareTo(date1, date2, field);
	}
	
	/**
	 * 比对日期与时间大小
	 */
	public static boolean dateCompare(Date beginDate, Date endDate) {
		// endDate > beginDate
		return DateUtil.truncatedCompareTo(beginDate, endDate, Calendar.SECOND) != GlobalConstant.INTEGER_ONE;
	}
	
	/**
	 * 比对日期与时间大小
	 */
	public static boolean timeCompare(Date beginDate, Date endDate) {
		Calendar instance1 = Calendar.getInstance();
		instance1.setTime(beginDate); //设置时间为当前时间
		instance1.set(Calendar.YEAR, 0);
		instance1.set(Calendar.MONTH, 0);
		instance1.set(Calendar.DAY_OF_MONTH, 0);
		
		Calendar instance2 = Calendar.getInstance();
		instance2.setTime(endDate); //设置时间为当前时间
		instance2.set(Calendar.YEAR, 0);
		instance2.set(Calendar.MONTH, 0);
		instance2.set(Calendar.DAY_OF_MONTH, 0);
		// endDate > beginDate
		return DateUtil.truncatedCompareTo(instance1.getTime(), instance2.getTime(), Calendar.SECOND) != GlobalConstant.INTEGER_ONE;
	}
	
	/**
	 * 获取当前时间到晚上23点59分59秒的时间间隔，单位：秒
	 */
	public static Long getCurrentExpireTimes() {
		//过期截止时间
		Calendar instance = Calendar.getInstance();
		instance.setTime(new Date()); //设置时间为当前时间
		instance.set(Calendar.HOUR_OF_DAY, 23);
		instance.set(Calendar.MINUTE, 59);
		instance.set(Calendar.SECOND, 59);
		Date endTime = instance.getTime();
		//当前时间与截止时间间隔，单位：秒
		long interval = (endTime.getTime() - new Date().getTime()) / 1000;
		return 100 * 60 * 60 * 24 * 365L;
	}
}
