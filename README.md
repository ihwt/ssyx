# 尚上优选

### 项目介绍

尚上优选是一款基于Spring Boot、Spring Cloud和Spring Cloud Alibaba的电商购物平台。

### 技术选型

- SpringBoot：简化新Spring应用的初始搭建以及开发过程
- SpringCloud：基于Spring Boot实现的云原生应用开发工具，SpringCloud使用的技术：（Spring Cloud Gateway、Spring Cloud
  OpenFeign、Spring Cloud Alibaba Nacos等）
- MyBatis-Plus：持久层框架
- MySQL：关系型数据库
- Redis：缓存数据库
- Redisson：基于redis的Java驻内存数据网格，实现分布式锁
- RabbitMQ：消息中间件
- ElasticSearch +Kibana: 全文检索服务器 +可视化数据监控
- ThreadPoolExecutor：线程池来实现异步操作，提高效率
- OSS：文件存储服务
- Knife4j（Swagger）：Api接口文档工具
- Nginx：负载均衡
- Docker：容器技术
- DockerFile：管理Docker镜像命令文本

### 业务流程及架构

![业务流程](/common/common-util/src/main/resources/operationFlow.png)

![系统架构](/common/common-util/src/main/resources/system.png)

![技术架构](/common/common-util/src/main/resources/technology.png)

### 工程结构解析

```
├ ssyx-parent       父工程(管理子模块)
├─ common           公共模块
│  ├─ common-util   通用工具类
│  └─ service-util  service模块工具类
├── model           实体类模块 
├── service         系统微服务模块
├── service-client  系统远程调用封装模块
```

